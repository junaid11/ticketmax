webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n<ng4-loading-spinner [loadingText]=\"'ticketMax'\"></ng4-loading-spinner>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
        this.title = 'app';
    }
    // tslint:disable-next-line:use-life-cycle-interface
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_image_upload__ = __webpack_require__("../../../../angular2-image-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_ui_switch__ = __webpack_require__("../../../../ngx-ui-switch/ui-switch.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular4_paystack__ = __webpack_require__("../../../../angular4-paystack/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__auth_auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__auth_guards_auth_guard__ = __webpack_require__("../../../../../src/app/auth/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__auth_guards_route_guard__ = __webpack_require__("../../../../../src/app/auth/guards/route.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__auth_guards_event_guard__ = __webpack_require__("../../../../../src/app/auth/guards/event.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_guards_user_guard__ = __webpack_require__("../../../../../src/app/auth/guards/user.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_pricing_pricing_component__ = __webpack_require__("../../../../../src/app/components/pricing/pricing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_ticket_ticket_component__ = __webpack_require__("../../../../../src/app/components/ticket/ticket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_browse_events_browse_events_component__ = __webpack_require__("../../../../../src/app/components/browse-events/browse-events.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_login_user_login_user_component__ = __webpack_require__("../../../../../src/app/components/login-user/login-user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_list_tickets_list_tickets_component__ = __webpack_require__("../../../../../src/app/components/list-tickets/list-tickets.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_register_user_register_user_component__ = __webpack_require__("../../../../../src/app/components/register-user/register-user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_sign_user_sign_user_component__ = __webpack_require__("../../../../../src/app/components/sign-user/sign-user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_login_org_login_org_component__ = __webpack_require__("../../../../../src/app/components/login-org/login-org.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_register_org_register_org_component__ = __webpack_require__("../../../../../src/app/components/register-org/register-org.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_sign_org_sign_org_component__ = __webpack_require__("../../../../../src/app/components/sign-org/sign-org.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_create_events_create_events_component__ = __webpack_require__("../../../../../src/app/components/create-events/create-events.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__components_navbar_fixed_navbar_fixed_component__ = __webpack_require__("../../../../../src/app/components/navbar-fixed/navbar-fixed.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_footer_footer_component__ = __webpack_require__("../../../../../src/app/components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__components_sidenav_sidenav_component__ = __webpack_require__("../../../../../src/app/components/sidenav/sidenav.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__components_event_detail_event_detail_component__ = __webpack_require__("../../../../../src/app/components/event-detail/event-detail.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









// import { JwtModule } from '@auth0/angular-jwt';

























var uri = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_16__components_home_home_component__["a" /* HomeComponent */] },
    { path: 'pricing', component: __WEBPACK_IMPORTED_MODULE_17__components_pricing_pricing_component__["a" /* PricingComponent */] },
    { path: 'block', component: __WEBPACK_IMPORTED_MODULE_18__components_ticket_ticket_component__["a" /* TicketComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_10__auth_guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'ticket/:id', component: __WEBPACK_IMPORTED_MODULE_18__components_ticket_ticket_component__["a" /* TicketComponent */] },
    { path: 'browse_events', component: __WEBPACK_IMPORTED_MODULE_19__components_browse_events_browse_events_component__["a" /* BrowseEventsComponent */] },
    { path: 'user-login', component: __WEBPACK_IMPORTED_MODULE_20__components_login_user_login_user_component__["a" /* LoginUserComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__auth_guards_route_guard__["a" /* RouteGuard */]] },
    { path: 'org-login', component: __WEBPACK_IMPORTED_MODULE_24__components_login_org_login_org_component__["a" /* LoginOrgComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__auth_guards_route_guard__["a" /* RouteGuard */]] },
    { path: 'create-events', component: __WEBPACK_IMPORTED_MODULE_27__components_create_events_create_events_component__["a" /* CreateEventsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_12__auth_guards_event_guard__["a" /* EventGuard */]] },
    { path: 'tickets', component: __WEBPACK_IMPORTED_MODULE_21__components_list_tickets_list_tickets_component__["a" /* ListTicketsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guards_user_guard__["a" /* UserGuard */]] },
    { path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_31__components_dashboard_dashboard_component__["a" /* DashboardComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_12__auth_guards_event_guard__["a" /* EventGuard */]] },
    { path: 'eventDetails/:id', component: __WEBPACK_IMPORTED_MODULE_33__components_event_detail_event_detail_component__["a" /* EventDetailComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_12__auth_guards_event_guard__["a" /* EventGuard */]] },
    { path: '**', redirectTo: '' }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_17__components_pricing_pricing_component__["a" /* PricingComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_ticket_ticket_component__["a" /* TicketComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_browse_events_browse_events_component__["a" /* BrowseEventsComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_login_user_login_user_component__["a" /* LoginUserComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_list_tickets_list_tickets_component__["a" /* ListTicketsComponent */],
                __WEBPACK_IMPORTED_MODULE_22__components_register_user_register_user_component__["a" /* RegisterUserComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_sign_user_sign_user_component__["a" /* SignUserComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_login_org_login_org_component__["a" /* LoginOrgComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_register_org_register_org_component__["a" /* RegisterOrgComponent */],
                __WEBPACK_IMPORTED_MODULE_26__components_sign_org_sign_org_component__["a" /* SignOrgComponent */],
                __WEBPACK_IMPORTED_MODULE_27__components_create_events_create_events_component__["a" /* CreateEventsComponent */],
                __WEBPACK_IMPORTED_MODULE_28__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_29__components_navbar_fixed_navbar_fixed_component__["a" /* NavbarFixedComponent */],
                __WEBPACK_IMPORTED_MODULE_30__components_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_31__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_32__components_sidenav_sidenav_component__["a" /* SidenavComponent */],
                __WEBPACK_IMPORTED_MODULE_33__components_event_detail_event_detail_component__["a" /* EventDetailComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forRoot(uri),
                __WEBPACK_IMPORTED_MODULE_6_angular2_image_upload__["a" /* ImageUploadModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_7_ngx_ui_switch__["a" /* UiSwitchModule */],
                __WEBPACK_IMPORTED_MODULE_8_angular4_paystack__["a" /* Angular4PaystackModule */],
                __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__["Ng4LoadingSpinnerModule"].forRoot(),
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_14__services_data_service__["a" /* DataService */],
                __WEBPACK_IMPORTED_MODULE_9__auth_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_10__auth_guards_auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_11__auth_guards_route_guard__["a" /* RouteGuard */],
                __WEBPACK_IMPORTED_MODULE_12__auth_guards_event_guard__["a" /* EventGuard */],
                __WEBPACK_IMPORTED_MODULE_13__auth_guards_user_guard__["a" /* UserGuard */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/auth/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthService = (function () {
    function AuthService() {
        this.user = localStorage.getItem('token');
        this.userType = localStorage.getItem('userId');
        if (this.userType === 'user') {
            this.organCheck = false;
        }
        else {
            this.organCheck = true;
        }
    }
    AuthService.prototype.isAuthenticated = function () {
        if (this.user) {
            return true;
        }
        else {
            return false;
        }
    };
    AuthService.prototype.isOrganiser = function () {
        if (this.user) {
            if (this.organCheck) {
                return true;
            }
            return false;
        }
        else {
            return false;
        }
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "../../../../../src/app/auth/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        // console.log('here');
        if (!this.auth.isAuthenticated()) {
            this.router.navigate(['/org-login']);
            return false;
        }
        return true;
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "../../../../../src/app/auth/guards/event.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EventGuard = (function () {
    function EventGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    EventGuard.prototype.canActivate = function (next, state) {
        if (!this.auth.isOrganiser()) {
            this.router.navigate(['/browse_events']);
            return false;
        }
        return true;
    };
    EventGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], EventGuard);
    return EventGuard;
}());



/***/ }),

/***/ "../../../../../src/app/auth/guards/route.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RouteGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RouteGuard = (function () {
    function RouteGuard(route, auth, location) {
        this.route = route;
        this.auth = auth;
        this.location = location;
    }
    RouteGuard.prototype.canActivate = function (next, state) {
        if (this.auth.isAuthenticated()) {
            this.route.navigate(['/browse_events']);
            return true;
        }
        else {
            return true;
        }
    };
    RouteGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_3__angular_common__["Location"]])
    ], RouteGuard);
    return RouteGuard;
}());



/***/ }),

/***/ "../../../../../src/app/auth/guards/user.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__("../../../../../src/app/auth/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserGuard = (function () {
    function UserGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    UserGuard.prototype.canActivate = function (next, state) {
        if (!this.auth.isOrganiser()) {
            return true;
        }
        this.router.navigate(['/']);
        return false;
    };
    UserGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], UserGuard);
    return UserGuard;
}());



/***/ }),

/***/ "../../../../../src/app/components/browse-events/browse-events.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".nav>li>a:focus,\r\n.nav>li>a:hover {\r\n  background-color: #24242e;\r\n  color: white;\r\n}\r\n\r\n.btn.focus,\r\n.btn:focus,\r\n.btn:hover {\r\n  color: white;\r\n}\r\n\r\n.foot {\r\n  position: fixed;\r\n  left: 0;\r\n  bottom: 0;\r\n  width: 100%;\r\n  height: 25%;\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  #resbox {\r\n    width: 30%;\r\n    height: 29%;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  #sim {\r\n    padding-top: 10%;\r\n  }\r\n}\r\n\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/browse-events/browse-events.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"background-color: #f8f8fa; margin-top: -4%\">\n  <app-navbar-fixed></app-navbar-fixed>\n  <!-- End of navigation bar -->\n\n  <div class=\"container\" style=\"padding-top: 8%;\">\n    <h3 id=\"sim\" style=\"font-family: Roboto; font-weight: 700; margin-left: -10%; text-align: center; color: 24242e;\">Events</h3>\n\n    <div *ngFor=\"let event of events\" style=\"margin-top: 3%; color: #24242e; margin-bottom: 2%;\">\n      <div class=\"row\" style=\"cursor: pointer;\" (click)=\"sendData(event._id, event.image_url, event.date, event.name, event.location)\">\n        <div id=\"resbox\" class=\"col-md-4 col-xs-4\" style=\"width: 15%; height: 10%; padding-right: 0px;\">\n          <img src=\"{{event.ref_image_url}}\" alt=\"\" style=\"width: 100%; height: 100%; overflow: hidden;\">\n        </div>\n        <div class=\"col-md-8 col-xs-8\" style=\"background-color: white;\">\n          <div class=\"row\">\n            <div style=\"padding: 3px 20px;\">\n              <p style=\"margin-top: 2%\">{{event.date}} {{event.time}}<span style=\"font-weight: 700; font-family: Roboto; float: right\">₦ {{event.price}}</span></p>\n              <p style=\"font-family: Roboto; font-weight: 900;\">{{event.name}}</p>\n              <p style=\"margin-top: 8%\">{{event.location}} <span style=\"font-weight: 700; font-family: Roboto; float: right\">\n                <img src=\"assets/icons/share.png\" style=\"height: 20px; width: 20px;\" alt=\"\">\n              </span></p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <!-- beginning of footer -->\n  <app-footer></app-footer>\n  <!-- end of footer -->\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/browse-events/browse-events.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrowseEventsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BrowseEventsComponent = (function () {
    function BrowseEventsComponent(dataService, route, router, spinnerService) {
        var _this = this;
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.spinnerService.show();
        this.dataService.getEvents().subscribe(function (events) {
            // console.log(events);
            _this.events = events;
            // tslint:disable-next-line:forin
            for (var key in _this.events) {
                _this.process_str = _this.events[key]['image_url'];
                _this.events[key]['ref_image_url'] = _this.process_str.split('.jpg')[0] + 'b.jpg';
            }
            _this.spinnerService.hide();
        });
    }
    BrowseEventsComponent.prototype.ngOnInit = function () {
    };
    BrowseEventsComponent.prototype.sendData = function (id, image, date, name, loc) {
        this.dataService.sendEvent(id, image, date, name, loc);
        this.router.navigate(['/ticket']);
    };
    BrowseEventsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-browse-events',
            template: __webpack_require__("../../../../../src/app/components/browse-events/browse-events.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/browse-events/browse-events.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], BrowseEventsComponent);
    return BrowseEventsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/create-events/create-events.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (max-width:600px) {\r\n  #butn {\r\n    float: left !important;\r\n    width: 100% !important;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:900px) {\r\n  #butm {\r\n    width: 40% !important;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #butnm {\r\n    width: 100% !important;\r\n  }\r\n}\r\n\r\n.blackBtn {\r\n  background-color: #24242e;\r\n  font-size: 15px;\r\n  color: white;\r\n  border-radius: 0px;\r\n  width: 50%;\r\n}\r\n\r\n.blackBtnTrans {\r\n  background-color: #24242e !important;\r\n  font-size: 15px;\r\n  color: white !important;\r\n  border-radius: 0px;\r\n  width: 50%;\r\n  float: right;\r\n}\r\n\r\n.whiteBtnTrans {\r\n  background-color: white !important;\r\n  font-size: 15px;\r\n  color: #24242e !important;\r\n  border-radius: 0px;\r\n  border: 1px solid #24242e;\r\n  width: 50%;\r\n}\r\n\r\n.whiteBtn {\r\n  background-color: white;\r\n  font-size: 15px;\r\n  float: right;\r\n  color: #24242e;\r\n  border-radius: 0px;\r\n  border: 1px solid #24242e;\r\n  width: 50%;\r\n}\r\n\r\n.hovering:hover {\r\n  cursor: pointer;\r\n}\r\n\r\n.hideIt {\r\n  display: none;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/create-events/create-events.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"container-fluid\" style=\"font-family: Roboto; color: #24242e; padding-left: 10%; padding-right: 10%;\">\n  <h2 style=\"margin-top: 3%; font-weight: 900\">{{userName}}</h2>\n  <p style=\"font-weight: 700\">Create Event</p>\n  <div style=\"margin-top: 3%\">\n    <form role=\"form\">\n      <div class=\"row\">\n        <div class=\"col-md-6 col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"title\">Event Title</label>\n            <input type=\"text\" class=\"form-control\" name=\"name\" [(ngModel)]=\"name\" id=\"name\">\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6 col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"location\">Location</label>\n            <input type=\"text\" class=\"form-control\" name=\"location\" [(ngModel)]=\"location\" id=\"location\">\n          </div>\n        </div>\n        <div class=\"col-md-4 col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"date\">Date</label>\n            <input type=\"date\" class=\"form-control\" name=\"dates\" [(ngModel)]=\"dates\" id=\"dates\">\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6 col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"time\">Time</label>\n            <input type=\"time\" class=\"form-control\" name=\"time\" [(ngModel)]=\"time\" id=\"time\">\n          </div>\n        </div>\n        <div class=\"col-sm-4 col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"number\">Contact Mobile Number</label>\n            <input type=\"tel\" class=\"form-control\" name=\"contact\" [(ngModel)]=\"contact\" id=\"contact\">\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6 col-xs-12\">\n          <div class=\"form-group\">\n            <label for=\"description\">Event Description</label>\n            <textarea class=\"form-control\" rows=\"5\" name=\"description\" [(ngModel)]=\"description\" id=\"description\"></textarea>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6 col-xs-12\">\n          <image-upload [class]=\"'customClass'\" [max]=\"1\" [url]=\"'https://api.imgur.com/3/upload'\"\n            [headers]=\"{Authorization: 'Client-ID f32179a1d2ffcc4'}\" [dropBoxMessage]=\"'Upload Event image'\"\n            (uploadFinished)=\"onUploadFinished($event)\">\n          </image-upload>\n        </div>\n      </div>\n      <h4 style=\"margin-top: 3%; font-weight: 900\">Ticket Details</h4>\n      <div class=\"row\" style=\"margin-top: 2%;\">\n        <div class=\"col-md-6 col-xs-6\">\n          <button [ngClass]=\"{'blackBtnTrans': black}\" (click)=\"checkFree()\" id=\"butn\" class=\"btn btn-lg whiteBtn\">Free Event</button>\n        </div>\n        <div class=\"col-md-6 col-xs-6\">\n          <button [ngClass]=\"{'whiteBtnTrans': black}\" (click)=\"checkPaid()\" class=\"btn btn-lg blackBtn\">Paid</button>\n        </div>\n      </div>\n      <div *ngFor=\"let item of items; let i=index;\" class=\"row\" style=\"margin-top: 4%;\">\n        <div [ngClass]=\"{'hideIt': hide[i]}\">\n          <div *ngIf=\"!paid\" class=\"col-md-1\"></div>\n          <div class=\"col-md-4 col-xs-12\">\n            <div class=\"form-group\">\n              <label for=\"ticket_name\">Ticket Name</label>\n              <input type=\"text\" class=\"form-control\" name=\"ticket_name_{{i}}\" [(ngModel)]=\"tickets[item].title\" id=\"ticket_name\">\n            </div>\n          </div>\n          <div class=\"col-md-4 col-xs-12\">\n            <div class=\"form-group\">\n              <label for=\"ticket_qty\">Ticket Quantity</label>\n              <input type=\"number\" class=\"form-control\" name=\"ticket_qty_{{i}}\" [(ngModel)]=\"tickets[item].limit\" id=\"ticket_qty\">\n            </div>\n          </div>\n          <div *ngIf=\"paid\" class=\"col-md-3 col-xs-12\">\n            <div class=\"form-group\">\n              <label for=\"ticket_price\">Ticket Price</label>\n              <input type=\"ticket\" class=\"form-control\" name=\"ticket_price_{{i}}\" [(ngModel)]=\"tickets[item].price\" id=\"ticket_price\">\n            </div>\n          </div>\n          <div *ngIf=\"items.length > 1\" (click)=\"removeTicket(i)\" class=\"col-md-1 col-xs-12 hovering\" style=\"margin-top: 1.5%; text-align: center;\">\n            <img src=\"assets/icons/delete.png\" width=\"40px\" height=\"40px\">\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" style=\"margin-top: 4%; text-align: center\">\n        <button (click)=\"addTicket()\" id=\"butm\" class=\"btn btn-lg\" style=\"background-color: white; font-size: 15px; color: #24242e; border-radius: 0px; border: 1px solid #24242e;\">+ &nbsp; Add Ticket</button>\n      </div>\n      <div class=\"row\" style=\"margin-top: 10%; margin-bottom: 4%; text-align: center\">\n        <button id=\"butnm\" (click)=\"goLive()\" class=\"btn btn-lg\" style=\"background-color: #ef4a4e; font-size: 15px; color: white; border-radius: 0px; width: 50%;\">Go Live!</button>\n      </div>\n    </form>\n  </div>\n</div>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "../../../../../src/app/components/create-events/create-events.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateEventsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateEventsComponent = (function () {
    function CreateEventsComponent(dataService, route, router, spinnerService) {
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.black = false;
        this.paid = true;
        this.tickets = [];
        this.hide = [];
        this.listOfPrice = [];
        this.items = [0];
        this.iter = 0;
        this.tickets = [{ 'title': '', 'limit': 0, 'price': '0' }];
        // console.log(this.tickets[0]['ticket_qty']);
        this.userName = localStorage.getItem('name');
    }
    CreateEventsComponent.prototype.ngOnInit = function () {
    };
    CreateEventsComponent.prototype.onUploadFinished = function ($event) {
        this.image = JSON.parse($event.serverResponse._body).data.link;
        // console.log(this.image);
    };
    CreateEventsComponent.prototype.addTicket = function () {
        ++this.iter;
        this.items.push(this.iter);
        this.hide.push(false);
        this.tickets.push({ 'title': '', 'limit': 0, 'price': '0' });
    };
    CreateEventsComponent.prototype.removeTicket = function (i) {
        // console.log(i);
        if (this.tickets.length > 1) {
            this.tickets[i] = 1;
            this.hide[i] = true;
        }
    };
    CreateEventsComponent.prototype.checkFree = function () {
        this.black = !this.black;
        this.paid = !this.paid;
        // make price = 0
        // this.ticket_price = '0';
    };
    CreateEventsComponent.prototype.checkPaid = function () {
        this.black = !this.black;
        this.paid = !this.paid;
    };
    CreateEventsComponent.prototype.getEventPrice = function () {
        for (var index = 0; index < this.tickets.length; index++) {
            this.listOfPrice.push(Number(this.tickets[index]['price']));
        }
        this.eventPrice = Math.min.apply(null, this.listOfPrice);
        console.log(this.eventPrice);
    };
    CreateEventsComponent.prototype.cleanArray = function () {
        for (var index = 0; index < this.tickets.length; index++) {
            if (this.tickets[index] === 1) {
                this.tickets.splice(index, 1);
            }
        }
    };
    CreateEventsComponent.prototype.cleanDate = function () {
        var day = this.dates.split('-')[2];
        var month = this.dates.split('-')[1];
        var year = this.dates.split('-')[0];
        this.date = day + '/' + month + '/' + year;
    };
    CreateEventsComponent.prototype.goLive = function () {
        var _this = this;
        this.spinnerService.show();
        this.getEventPrice();
        this.cleanArray();
        this.cleanDate();
        var obj = {
            name: this.name,
            location: this.location,
            time: this.time,
            price: this.eventPrice,
            image_url: this.image,
            date: this.date,
            table: this.tickets,
            contact: this.contact,
            description: this.description
        };
        this.dataService.createEvents(obj).subscribe(function (res) {
            // console.log(res);
            if (res.status === true) {
                _this.router.navigate(['/dashboard']);
                _this.spinnerService.hide();
            }
            _this.spinnerService.hide();
        });
    };
    CreateEventsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-create-events',
            template: __webpack_require__("../../../../../src/app/components/create-events/create-events.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/create-events/create-events.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], CreateEventsComponent);
    return CreateEventsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* Centered text */\r\n\r\n.name {\r\n  position: absolute;\r\n  top: 1%;\r\n  left: 15%;\r\n}\r\n\r\n/* Bottom left text */\r\n\r\n.date {\r\n  position: absolute;\r\n  top: 40%;\r\n  left: 15%;\r\n}\r\n\r\n/* Top left text */\r\n\r\n.btnm {\r\n  position: absolute;\r\n  top: 60%;\r\n  left: 40%;\r\n}\r\n\r\n.blacky:hover {\r\n  background-color: #24242e;\r\n}\r\n\r\n.blacky {\r\n  background-color: white;\r\n}\r\n\r\n@media only screen and (max-width: 1024px) {\r\n  /* div h1 {\r\n    margin-top: -50px;\r\n  } */\r\n  [class=\"adjust\"] {\r\n    margin: 0% 8% 8% 0% !important;\r\n  }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"background-color: #f4f8f9; height: 100%\">\n  <div style=\"box-shadow: 0px 2px 16px 0px #e5e5e7; margin-bottom: 0px;\">\n    <nav class=\"navbar\" style=\"background-color: white\">\n      <div class=\"container-fluid\">\n        <div class=\"navbar-header\">\n          <a [routerLink]=\"[ '/home' ]\">\n            <img id=\"menu-img\" style=\"margin-top: 10px; margin-left: 10px; width: 10%; height: 5%; margin-bottom: 1%;\" class=\"img-responsive\" src=\"assets/images/gatepass-dark.png\">\n          </a>\n        </div>\n      </div>\n    </nav>\n  </div>\n  <div style=\"margin-top: 0px;\">\n    <div class=\"row\" style=\"margin:  2% 5% 5% 5%;\">\n      <div class=\"col-md-3 col-xs-12\">\n        <div style=\"background-color: white; height: 100%; width: 80%; margin: auto; margin-top: 1%; padding-top: 8%; padding-bottom: 36%; margin-bottom: 10%; box-shadow: 0px 2px 16px 0px #e5e5e7;\">\n          <h4 style=\"color: #24242e; font-family: Roboto; text-align: center;font-weight: 700;\">Welcome {{name}}</h4>\n          <p style=\"color: #cdcdcd; font-family: Roboto; font-weight: 600; margin-top: 10%; text-align: center;\">{{email}}</p>\n\n\n          <div style=\"margin-top: 18%; cursor: pointer;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%;\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/Home.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <a [routerLink]=\"['/dashboard']\" style=\"text-decoration: none;\">\n                  <span style=\"color: #24242e; font-weight: 700;\">Events</span>\n                </a>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 6%; cursor: pointer;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div class=\"blacky\" style=\"height: 50%; width: 2.8%; padding: 5% 0% 5% 0%;\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/calw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <a [routerLink]=\"['/create-events']\" style=\"text-decoration: none;\">\n                  <span style=\"color: #cdcdcd; font-weight: 700;\">Create Events</span>\n                </a>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 6%; cursor: pointer;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div class=\"blacky\" style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%; visibility: hidden\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/setw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <span style=\"color: #cdcdcd; font-weight: 700;\">Settings</span>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 6%; cursor: pointer;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div class=\"blacky\" style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%; visibility: hidden\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/userw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <span style=\"color: #cdcdcd; font-weight: 700;\">Profile</span>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 50%; cursor: pointer;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div class=\"blacky\" style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%; visibility: hidden\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/shutw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\" (click)=\"logout()\">\n                <span style=\"color: #cdcdcd; font-weight: 700;\">Sign Out</span>\n              </div>\n            </div>\n          </div>\n\n        </div>\n      </div>\n      <div class=\"col-md-8 col-xs-12\" style=\"padding: 3%;\">\n        <h3 style=\"color: #24242e; font-family: Roboto; font-weight: 700;\">Events</h3>\n        <div class=\"row\" style=\"margin-top: 5%\">\n          <div class=\"adjust\" *ngFor=\"let event of listEvents\" class=\"col-md-3 col-xs-12\" style=\"margin: 0% 8% 8% 0%\">\n            <div style=\"background-color: white; width: 250px; height: 150px;\">\n              <img src=\"{{event.image_url}}\" style=\"opacity: 0.3; width: 250px; height: 150px;\" alt=\"\">\n              <div class=\"name\">\n                <h3 style=\"color: #24242e; font-family: Roboto; font-weight: 900\">{{event.name}}</h3>\n              </div>\n              <div class=\"date\">\n                <p style=\"color: #24242e; font-family: Roboto;\">{{event.date}} {{event.time}}</p>\n              </div>\n              <div class=\"btnm\">\n                <div (click)=\"showEvent(event.open_url)\" style=\"border: 1px solid #24242e; border-radius: 16px; padding: 2px 25px 2px 25px; cursor: pointer;\">View</div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DashboardComponent = (function () {
    function DashboardComponent(dataService, spinnerService, route, router) {
        var _this = this;
        this.dataService = dataService;
        this.spinnerService = spinnerService;
        this.route = route;
        this.router = router;
        this.spinnerService.show();
        this.name = localStorage.getItem('name');
        this.email = localStorage.getItem('email');
        this.dataService.displayEvents().subscribe(function (events) {
            _this.spinnerService.hide();
            _this.listEvents = events;
        });
        // console.log(this.listEvents[0]);
    }
    DashboardComponent.prototype.ngOnInit = function () {
        // this.spinnerService.show();
    };
    DashboardComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/']);
    };
    DashboardComponent.prototype.showEvent = function (id) {
        // this.dataService.id = id;
        this.router.navigate(['/eventDetails', id]);
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/event-detail/event-detail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* The switch - the box around the slider */\r\n\r\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 25px;\r\n}\r\n\r\n/* Hide default HTML checkbox */\r\n\r\n.switch input {\r\n  display: none;\r\n}\r\n\r\n/* The slider */\r\n\r\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: .4s;\r\n}\r\n\r\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 16px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: .4s;\r\n}\r\n\r\ninput:checked+.slider {\r\n  background-color: #69e3bd;\r\n}\r\n\r\ninput:focus+.slider {\r\n  box-shadow: 0 0 1px #69e3bd;\r\n}\r\n\r\ninput:checked+.slider:before {\r\n  transform: translateX(26px);\r\n}\r\n\r\n/* Rounded sliders */\r\n\r\n.slider.round {\r\n  border-radius: 80px;\r\n}\r\n\r\n.slider.round:before {\r\n  border-radius: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/event-detail/event-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"background-color: #f4f8f9; height: 100%\">\n  <div style=\"box-shadow: 0px 2px 16px 0px #e5e5e7; margin-bottom: 0px;\">\n    <nav class=\"navbar\" style=\"background-color: white\">\n      <div class=\"container-fluid\">\n        <div class=\"navbar-header\">\n          <a [routerLink]=\"[ '/home' ]\">\n            <img id=\"menu-img\" style=\"margin-top: 10px; margin-left: 10px; width: 10%; height: 5%; margin-bottom: 1%;\" class=\"img-responsive\"\n              src=\"assets/images/gatepass-dark.png\">\n          </a>\n        </div>\n      </div>\n    </nav>\n  </div>\n  <div style=\"margin-top: 0px;\">\n    <div class=\"row\" style=\"margin:  2% 5% 5% 5%;\">\n      <div class=\"col-md-3 col-xs-12\">\n        <div style=\"background-color: white; height: 100%; width: 80%; margin: auto; margin-top: 1%; padding-top: 8%; padding-bottom: 135%; margin-bottom: 10%; box-shadow: 0px 2px 16px 0px #e5e5e7;\">\n          <h4 style=\"color: #24242e; font-family: Roboto; text-align: center;font-weight: 700;\">Welcome {{name}}</h4>\n          <p style=\"color: #cdcdcd; font-family: Roboto; font-weight: 600; margin-top: 10%; text-align: center;\">{{email}}</p>\n\n\n          <div style=\"margin-top: 18%;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%;\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/Home.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <a [routerLink]=\"['/dashboard']\" style=\"text-decoration: none;\">\n                  <span style=\"color: #24242e; font-weight: 700;\">Events</span>\n                </a>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 6%;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%; visibility: hidden;\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/calw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <a [routerLink]=\"['/create-events']\" style=\"text-decoration: none;\">\n                  <span style=\"color: #cdcdcd; font-weight: 700;\">Create Events</span>\n                </a>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 6%;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%; visibility: hidden\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/setw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <span style=\"color: #cdcdcd; font-weight: 700;\">Settings</span>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 6%;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%; visibility: hidden\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/userw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\">\n                <span style=\"color: #cdcdcd; font-weight: 700;\">Profile</span>\n              </div>\n            </div>\n          </div>\n\n          <div style=\"margin-top: 50%;\">\n            <div style=\"display: inline-flex; width: 100%;\">\n              <div style=\"background-color: #24242e; height: 50%; width: 2.8%; padding: 5% 0% 5% 0%; visibility: hidden\"></div>\n              <div style=\"margin-left: 5%;\">\n                <img src=\"assets/icons/shutw.png\" style=\"height: 20px; width: 20px;\">\n              </div>\n              <div style=\"margin-left: 10%; width: 80%\" (click)=\"logout()\">\n                <span style=\"color: #cdcdcd; font-weight: 700;\">Sign Out</span>\n              </div>\n            </div>\n          </div>\n\n        </div>\n      </div>\n      <!-- Second Section -->\n      <div *ngIf=\"event_details\">\n        <div class=\"col-md-8 col-xs-12\" style=\"padding: 2%;\">\n          <div>\n            <h3 style=\"color: #24242e; font-family: Roboto; font-weight: 700; display: inline-flex\">Ticket Sales For {{event_details.name}}</h3>\n            <span style=\"float: right;\">\n              <button class=\"btn btn-md\" style=\"background-color: #ef4a4e; color: white; margin-top: 20% !important; padding-left: 30px; padding-right: 30px; border-radius: 0px;\">Cash Out</button>\n            </span>\n          </div>\n\n          <div style=\"margin-top: 5%\">\n            <div class=\"row\">\n              <div class=\"col-md-4 col-xs-12\">\n                <div style=\"background-color: white; padding: 5% 20% 5% 20%; text-align: center; font-family: Roboto; color: #24242e; box-shadow: 0px 2px 16px 0px #e5e5e7; margin: 0% 8% 8% 0%;\">\n                  <h4 style=\"font-weight: 700\">Tickets Sold</h4>\n                  <h2 style=\"color: #cdcdcd; margin-bottom: 0%;\">{{event_details.total_sold}}</h2>\n                </div>\n              </div>\n              <div class=\"col-md-4 col-xs-12\">\n                <div style=\"background-color: white; padding: 5% 20% 5% 20%; text-align: center; font-family: Roboto; color: #24242e; box-shadow: 0px 2px 16px 0px #e5e5e7; margin: 0% 8% 8% 0%;\">\n                  <h4 style=\"font-weight: 700\">Tickets Available</h4>\n                  <h2 style=\"color: #cdcdcd; margin-bottom: 0%;\">10</h2>\n                </div>\n              </div>\n              <div class=\"col-md-4 col-xs-12\">\n                <div style=\"background-color: white; padding: 5% 20% 5% 20%; text-align: center; font-family: Roboto; color: #24242e; box-shadow: 0px 2px 16px 0px #e5e5e7; margin: 0% 8% 8% 0%;\">\n                  <h4 style=\"font-weight: 700\">Total Sales</h4>\n                  <h2 style=\"color: #cdcdcd; margin-bottom: 0%;\">₦{{event_details.gross_sales}}</h2>\n                </div>\n              </div>\n            </div>\n            <div class=\"row\">\n              <div class=\"col-md-12 col-xs-12\">\n                <div style=\"background-color: white; padding: 1% 20% 5% 3%; font-family: Roboto; color: #24242e; box-shadow: 0px 2px 16px 0px #e5e5e7;\">\n                  <h4 style=\"font-weight: 700; display: inline-flex;\">Event Summary</h4>\n                  <div style=\"display: inline-flex; margin-left: 3%; border: 1px solid #24242e; border-radius: 8px; padding-left: 20px; padding-right: 20px; padding-top: -2%;\">Share</div>\n                  <div style=\"display: inline-flex; background-color: #24242e; color: white; margin-left: 3%; border: 1px solid #24242e; border-radius: 8px; padding-left: 20px; padding-right: 20px; padding-top: -2%;\">Edit Ticket</div>\n                  <div class=\"row\" style=\"margin-top: 5%\">\n                    <p class=\"col-md-2 col-xs-12\">Event Name: </p>\n                    <p class=\"col-md-4 col-xs-12\">{{event_details.name}}</p>\n                  </div>\n                  <div class=\"row\">\n                    <p class=\"col-md-2 col-xs-12\">Event Date: </p>\n                    <p class=\"col-md-4 col-xs-12\">{{event_details.date}} {{event_details.time}}</p>\n                  </div>\n                  <div class=\"row\">\n                    <p class=\"col-md-2 col-xs-12\">Event Location: </p>\n                    <p class=\"col-md-4 col-xs-12\"> {{event_details.location}} </p>\n                  </div>\n                  <div class=\"row\">\n                    <p class=\"col-md-2 col-xs-12\">Event Summary: </p>\n                    <p class=\"col-md-4 col-xs-12\"> {{event_details.description}} </p>\n                  </div>\n                  <div class=\"row\">\n                    <p class=\"col-md-2 col-xs-4\">Artwork: </p>\n                    <div class=\"col-md-4 col-xs-4\">\n                      <img src=\"{{event_details.image_url}}\" style=\"width: 100px; height: 120px;\" alt=\"\">\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"row\" style=\"margin-top: 2%\">\n              <div class=\"col-md-12 col-xs-12\">\n                <div style=\"background-color: white; padding: 1% 3% 5% 3%; font-family: Roboto; color: #24242e; box-shadow: 0px 2px 16px 0px #e5e5e7;\">\n                  <h4 style=\"font-weight: 700\">Tickets</h4>\n                  <table class=\"table\">\n                    <thead>\n                      <tr>\n                        <th>Ticket Type</th>\n                        <th>Price</th>\n                        <th>Available</th>\n                        <th>Online</th>\n                        <th></th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr *ngFor=\"let item of event_details.table\">\n                        <td>{{item.title}}</td>\n                        <td>{{item.price}}</td>\n                        <td>{{item.limit}}</td>\n                        <td>\n                          <label class=\"switch\">\n                            <input type=\"checkbox\">\n                            <!-- <span class=\"slider round\"></span> -->\n                            <ui-switch [(ngModel)]=\"item.is_online\" (click)=\"switch(item._id['$oid'])\"></ui-switch>\n                          </label>\n                        </td>\n                        <td style=\"cursor: pointer; font-weight: 700\" data-toggle=\"modal\" data-target=\"#myModal\" (click)=\"getTableDetails(item.title, item.price, item.limit, item._id['$oid'])\">Edit Ticket</td>\n                      </tr>\n                    </tbody>\n                  </table>\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <!-- Modal -->\n          <div id=\"myModal\" class=\"modal fade\" role=\"dialog\" style=\"margin-top: 7%;\">\n            <div class=\"modal-dialog\">\n              <!-- Modal content-->\n              <div class=\"modal-content\">\n                <div class=\"modal-header\" style=\"background-color: #d8d8d8; border-radius: 0px;\">\n                  <h4 class=\"modal-title\" style=\"text-align: center; font-weight: 700; font-family: Roboto\">Edit Tickets</h4>\n                </div>\n                <div>\n                  <div class=\"modal-body\" style=\"font-family: Roboto\">\n                    <div style=\"text-align: center; padding: 2% 10% 5% 10%\">\n                      <form role=\"form\">\n                        <div class=\"row\" style=\"margin-top: 2%\">\n                          <div class=\"col-md-12 col-xs-12\">\n                            <div class=\"form-group\">\n                              <label for=\"title\">Ticket Type</label>\n                              <input type=\"text\" class=\"form-control\" name=\"title\" [(ngModel)]=\"title\" id=\"title\">\n                            </div>\n                          </div>\n                        </div>\n                        <div class=\"row\" style=\"margin-top: 2%\">\n                          <div class=\"col-md-12 col-xs-12\">\n                            <div class=\"form-group\">\n                              <label for=\"title\">Price</label>\n                              <input type=\"text\" class=\"form-control\" name=\"price\" [(ngModel)]=\"price\" id=\"price\">\n                            </div>\n                          </div>\n                        </div>\n                        <div class=\"row\" style=\"margin-top: 2%\">\n                          <div class=\"col-md-12 col-xs-12\">\n                            <div class=\"form-group\">\n                              <label for=\"title\">Available</label>\n                              <input type=\"text\" class=\"form-control\" name=\"limit\" [(ngModel)]=\"limit\" id=\"limit\">\n                            </div>\n                          </div>\n                        </div>\n\n                        <div class=\"row\" style=\"margin-top: 4%; text-align: center\">\n                          <button (click)=\"sendTableDetails()\" class=\"btn btn-md\" style=\"background-color: #ef4a4e; font-size: 15px; color: white; border-radius: 4px;\" data-dismiss=\"modal\">Save Changes</button>\n                        </div>\n                      </form>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"modal-footer\" style=\"background-color: #d8d8d8; border-radius: 0px; padding-top: 3%;\">\n                  <button type=\"button\" class=\"btn\" style=\"background-color: #ef4a4e; color: white; font-weight: 700;\" data-dismiss=\"modal\">Close</button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/event-detail/event-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EventDetailComponent = (function () {
    function EventDetailComponent(dataService, route, router, spinnerService) {
        var _this = this;
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.spinnerService.show();
        this.email = localStorage.getItem('email');
        this.name = localStorage.getItem('name');
        this.dataService.getEventDetails(this.route.snapshot.paramMap.get('id')).subscribe(function (res) {
            // console.log(res.table);
            _this.event_details = res;
            _this.spinnerService.hide();
        });
    }
    EventDetailComponent.prototype.ngOnInit = function () {
    };
    EventDetailComponent.prototype.getTableDetails = function (title, price, available, id) {
        this.title = title;
        this.price = price;
        this.limit = available;
        this.id = id;
    };
    EventDetailComponent.prototype.sendTableDetails = function () {
        var obj = {
            title: this.title,
            price: this.price,
            limit: this.limit
        };
        this.dataService.sendTable(this.id, obj).subscribe(function (res) {
            console.log(res);
        });
    };
    EventDetailComponent.prototype.switch = function (id) {
        this.dataService.toggleOn(id).subscribe(function (res) {
            console.log(res);
        });
    };
    EventDetailComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/']);
    };
    EventDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-event-detail',
            template: __webpack_require__("../../../../../src/app/components/event-detail/event-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/event-detail/event-detail.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], EventDetailComponent);
    return EventDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"foot\" style=\"margin-top: 100px; background-color: #24242e\">\n  <div class=\"container-fluid\" style=\"font-family: Roboto;\">\n    <div class=\"container\">\n      <div class=\"row\" style=\"margin-top: 2%;\">\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">ticketMax</li>\n            <a style=\"text-decoration: none;\" [routerLink]=\"[ '/home' ]\">\n              <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Home</li>\n            </a>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Events</li>\n          </ul>\n        </div>\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">Contact</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">FAQ</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Contact Us</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Facebook</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Twitter</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Instagram</li>\n          </ul>\n        </div>\n        <div class=\"col-md-4 col-xs-0\"></div>\n        <div class=\"col-md-4 col-xs-12\">\n          <img class=\"img-responsive\" src=\"assets/images/wt.png\" alt=\"\">\n        </div>\n      </div>\n      <div>\n        <hr style=\"color: #9c9ca0\">\n      </div>\n    </div>\n    <div class=\"row\" style=\"margin-top: -1%;\">\n      <div class=\"col-md-4 col-xs-4\"></div>\n      <div class=\"col-md-4 col-xs-4\" style=\"display: inline-flex;\">\n        <span style=\"margin-right: 15%; margin-left: 25%;\">\n          <img class=\"img-responsive\" width=\"30px\" height=\"30px\" src=\"assets/icons/facebook.png\" alt=\"\">\n        </span>\n        <span style=\"margin-right: 15%;\">\n          <img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/tweet.png\" alt=\"\">\n        </span>\n        <span style=\"margin-right: 15%;\">\n          <img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/insta.png\" alt=\"\">\n        </span>\n      </div>\n      <div class=\"col-md-4 col-xs-4\"></div>\n    </div>\n    <div style=\"text-align: center; font-family: Roboto; color: #9c9ca0; margin-top: 5%;\">\n      &copy;\n      <span>ticketMax, 2018</span>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#menu-img {\r\n  margin-top: 21% !important;\r\n  margin-left: 3% !important;\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #menu-img {\r\n    margin-top: 5% !important;\r\n    margin-left: 3% !important;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #adjust {\r\n    margin-top: 0% !important;\r\n    margin-left: 1% !important;\r\n  }\r\n}\r\n\r\n#boxed-img {\r\n  width: 220px;\r\n  height: 150px;\r\n}\r\n\r\n#boxed {\r\n  width: 220px;\r\n  height: 150px;\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #boxed-img {\r\n    width: 220px;\r\n    height: 150px;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #boxed {\r\n    margin-left: 20%;\r\n    width: 220px;\r\n    height: 150px;\r\n  }\r\n}\r\n\r\n\r\n.dropBtn {\r\n  background-color: #24242e !important;\r\n  color: white;\r\n  background-image: none !important;\r\n  border: none;\r\n  border-radius: 8px;\r\n}\r\n\r\n.btn-default.active.focus,\r\n.btn-default.active:focus,\r\n.btn-default.active:hover,\r\n.btn-default:active.focus,\r\n.btn-default:active:focus,\r\n.btn-default:active:hover,\r\n.open>.dropdown-toggle.btn-default.focus,\r\n.open>.dropdown-toggle.btn-default:focus,\r\n.open>.dropdown-toggle.btn-default:hover {\r\n  color: white !important;\r\n  background-color: transparent !important;\r\n  border-color: none !important;\r\n}\r\n\r\n@media (max-width: 767px) {\r\n  .navbar-nav .open .dropdown-menu {\r\n    background-color: white !important;\r\n  }\r\n}\r\n\r\n.nav>li>a:focus,\r\n.nav>li>a:hover {\r\n  background-color: #24242e;\r\n  color: white;\r\n}\r\n\r\n.btn.focus,\r\n.btn:focus,\r\n.btn:hover {\r\n  color: white;\r\n}\r\n\r\n\r\n.back {\r\n  background-image: url(" + escape(__webpack_require__("../../../../../src/assets/images/back.png")) + ");\r\n  background-size: 100% 100%;\r\n  background-repeat: no-repeat;\r\n  min-width: 100%;\r\n  min-height: 100%;\r\n  height: auto;\r\n  width: auto;\r\n  -o-object-fit: cover;\r\n     object-fit: cover;\r\n  background-position: center center;\r\n}\r\n\r\n\r\n\r\na:hover {\r\n  text-decoration: none;\r\n  color: black;\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  /* div h1 {\r\n    margin-top: -50px;\r\n  } */\r\n  [class=\"text\"] {\r\n    margin-top: -50px;\r\n  }\r\n}\r\n\r\n.jumb {\r\n  background-color: white;\r\n  margin: 10px 40px 180px 40px;\r\n  padding: 0px;\r\n  box-shadow: 0 2px 0 grey;\r\n}\r\n\r\n.adjust {\r\n  margin-left: -20%;\r\n}\r\n\r\n@media screen and (min-width:600px) {\r\n  #menu_btn {\r\n    display: none;\r\n  }\r\n  #menu_icon {\r\n    display: inline-block;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #menu_btn {\r\n    display: inline-block;\r\n  }\r\n  #menu_icon {\r\n    display: none;\r\n  }\r\n}\r\n\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"back\">\n  <nav class=\"navbar\" style=\"background-color: #24242e\">\n    <div class=\"container-fluid\">\n      <div class=\"navbar-header\">\n        <button type=\"button\" style=\"border-color: #9c9ca0\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n          <img src=\"assets/icons/menu.png\" width=\"20px\" height=\"20px\" alt=\"\">\n        </button>\n        <div id=\"menu-img\">\n          <a [routerLink]=\"[ '/' ]\">\n            <img class=\"img-responsive\" src=\"assets/images/tmsm.png\">\n          </a>\n        </div>\n      </div>\n      <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n        <ul class=\"nav navbar-nav navbar-right\">\n          <li>\n            <a [routerLink]=\"[ '/browse_events' ]\" style=\"color: white; font-weight: 600\">Browse Events</a>\n          </li>\n          <li>\n            <a [routerLink]=\"[ '/pricing' ]\" style=\"color: white; font-weight: 600\">Pricing</a>\n          </li>\n          <li *ngIf=\"user && !userToggle\">\n            <a [routerLink]=\"[ '/tickets' ]\" style=\"color: white; font-weight: 600\">Tickets</a>\n          </li>\n          <li>\n            <a *ngIf=\"!user\" [routerLink]=\"[ '/block' ]\" style=\"color: white;\">\n              <button class=\"btn btn-sm up-button\" style=\"margin-top: -2px; border-radius: 8px; background-color: #ef4a4e;\">Create Events</button>\n            </a>\n          </li>\n          <li>\n            <a *ngIf=\"userToggle && user\" [routerLink]=\"[ '/dashboard' ]\" style=\"color: white;\">\n              <button class=\"btn btn-sm up-button\" style=\"margin-top: -2px; border-radius: 8px; background-color: #ef4a4e;\">Dashboard</button>\n            </a>\n          </li>\n          <li *ngIf=\"!user\">\n            <div id=\"adjust\" class=\"dropdown\" style=\"margin-top: 13%;\">\n              <button class=\"btn btn-default dropdown-toggle dropBtn\" type=\"button\" id=\"login\" data-toggle=\"dropdown\">Login\n                <span class=\"caret\"></span>\n              </button>\n              <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"login\">\n                <li role=\"presentation\">\n                  <a role=\"menuitem\" [routerLink]=\"[ '/user-login' ]\">Login As A User</a>\n                </li>\n                <li class=\"divider\"></li>\n                <li role=\"presentation\">\n                  <a role=\"menuitem\" [routerLink]=\"[ '/org-login' ]\">Login As An Organiser</a>\n                </li>\n              </ul>\n            </div>\n          </li>\n          <li *ngIf=\"user\" style=\"cursor: pointer\">\n            <a (click)=\"logout()\" style=\"color: white; font-weight: 600\">Logout</a>\n          </li>\n        </ul>\n      </div>\n    </div>\n  </nav>\n\n  <div class=\"row\" style=\"margin-right: 0px;\">\n    <div class=\"col-md-3 col-xs-3\"></div>\n    <div class=\"col-md-3 col-xs-3\">\n      <img class=\"img-responsive\" src=\"assets/images/phone.png\" alt=\"\" style=\"margin-top: 40px; margin-bottom: 90px;\">\n    </div>\n    <div class=\"col-md-3 col-xs-3\" style=\"color: white; margin-top: 120px; font-weight: 900;\">\n      <h1 class=\"text\" style=\"font-size: 2vw; font-weight: 700;\">\n        ALL YOUR TICKETS,<br>\n        IN A SINGLE APP\n      </h1>\n      <p class=\"resize\" style=\"font-size: 1.5vw; font-weight: 300; font-family: Roboto;\">\n        Buy your ticket in a single app,<br>\n        save your time, fast payment, <br>\n        multiple choice\n      </p>\n    </div>\n    <div class=\"col-md-3 col-xs-3\"></div>\n  </div>\n</div>\n\n<div class=\"container\">\n  <!-- Beginning Of Boxes -->\n  <div style=\"margin-top: 35px;\">\n    <h4 style=\"text-align: center; font-family: Roboto; margin-bottom: 35px;\">Popular Events Around You</h4>\n    <div>\n      <div class=\"row\">\n        <div *ngFor=\"let event of events; let i = index\">\n          <div *ngIf=\"i<6\">\n            <a style=\"color: #24242e;\" (click)=\"reRoute(event.open_url)\">\n              <div id=\"boxed\" class=\"col-md-3 col-xs-12 jumb\">\n                <img id=\"boxed-img\" class=\"img-responsive\" src=\"{{event.ref_image_url}}\" alt=\"\" (click)=\"sendData(event._id, event.image_url, event.date, event.name, event.location)\">\n                <div style=\"margin-bottom: 10px; padding-bottom: 5px; box-shadow: 0px 6px 10px -5px grey;\">\n                  <div style=\"margin-top: 10px;\">\n                    <div class=\"row\">\n                      <span class=\"col-md-3 col-xs-3\"><img class=\"img-responsive\" width=\"30px\" height=\"30px\" src=\"assets/icons/calendar.png\" alt=\"\"></span>\n                      <span class=\"col-md-9 col-xs-7\"><p class=\"adjust\" style=\"margin-top: 5px;\">{{event.date}}</p></span>\n                    </div>\n                  </div>\n                  <div style=\"margin-top: 5px;\">\n                    <div class=\"row\">\n                      <span class=\"col-md-3 col-xs-3\"><img class=\"img-responsive\" width=\"30px\" height=\"30px\" src=\"assets/icons/place.png\" alt=\"\"></span>\n                      <span class=\"col-md-9 col-xs-7\"><p class=\"adjust\" style=\"margin-top: 5px; font-weight: 700;\">{{event.name}}</p></span>\n                    </div>\n                  </div>\n                  <div style=\"margin-top: 5px;\">\n                    <div class=\"row\">\n                      <span class=\"col-md-3 col-xs-3\"><img class=\"img-responsive\" width=\"30px\" height=\"30px\" src=\"assets/icons/location.png\" alt=\"\"></span>\n                      <span class=\"col-md-9 col-xs-7\"><p class=\"adjust\" style=\"margin-top: 5px; font-size: 13px;\">{{event.location}}</p></span>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </a>\n          </div>\n          <div *ngIf=\"i==6\">\n            <div style=\"text-align: center;\">\n              <button class=\"btn btn-lg\" (click)=\"moveAway()\" style=\"color: white; background-color: #ef4a4e; box-shadow: 2px 2px 8px 0px #ef4a4e;\">Show More</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  <!-- End Of Boxes -->\n  <!-- Beginning Of IPhone and App Stores -->\n  <div  style=\"margin-top: 100px;\">\n    <div class=\"row\">\n      <div class=\"col-md-2 col-xs-2\"></div>\n      <div class=\"col-md-4 col-xs-4\">\n        <img class=\"img-responsive\" src=\"assets/images/homephone.png\" alt=\"\">\n      </div>\n      <div class=\"col-md-4 col-xs-4\" style=\"margin-top: 5%;\">\n        <h1 style=\"font-size: 3vw; font-weight: 300; font-family: Roboto; margin-bottom: 15%;\">\n          Now Available\n        </h1>\n        <p style=\"font-size: 1.5vw; font-weight: 400; font-family: Roboto; color: #d1d1d2;\">\n          What are you waiting for?\n          <br> save your time, get your ticket!\n        </p>\n        <div class=\"row\" style=\"margin-top: 15%;\">\n          <div class=\"col-md-6 col-xs-6\">\n            <img class=\"img-responsive\" src=\"assets/images/ggplay.png\" alt=\"\">\n          </div>\n          <div class=\"col-md-6 col-xs-6\">\n            <img class=\"img-responsive\" src=\"assets/images/appstore.png\" alt=\"\">\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-2 col-xs-2\"></div>\n    </div>\n    <div style=\"margin-top: 100px;\">\n      <h4 style=\"text-align: center; font-weight: 400; font-family: Roboto; margin-bottom: 2%;\">Get Started</h4>\n      <h4 style=\"text-align: center; font-weight: 600; font-family: Roboto; color: #d1d1d2; margin-bottom: 4%;\">Enter your email address to get started</h4>\n      <div class=\"row\">\n        <div class=\"col-md-4 col-xs-0\"></div>\n        <div class=\"col-md-4 col-xs-8\">\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"text\" name=\"\" id=\"\" placeholder=\"Your email address\" style=\"background-color: #d1d1d2; color: white !important;\">\n          </div>\n        </div>\n        <div class=\"col-md-2 col-xs-4\">\n          <button class=\"btn btn-md\" style=\"color: white; background-color: #ef4a4e; box-shadow: 2px 8px 58px 0px #ef4a4e;\">I'm Ready</button>\n        </div>\n        <div class=\"col-md-2 col-xs-0\"></div>\n      </div>\n    </div>\n  </div>\n  <!-- End of Iphont and App Stores -->\n  </div>\n</div>\n\n<!-- Beginning Of Footer -->\n<div style=\"margin-top: 100px; background-color: #24242e\">\n  <div class=\"container-fluid\" style=\"font-family: Roboto;\">\n    <div class=\"container\">\n      <div class=\"row\" style=\"margin-top: 2%;\">\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">ticketMax</li>\n            <a [routerLink]=\"[ '/' ]\">\n              <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Home</li>\n            </a>\n            <a [routerLink]=\"[ '/browse_events' ]\">\n              <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Events</li>\n            </a>\n          </ul>\n        </div>\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">Contact</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">FAQ</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Contact Us</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Facebook</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Twitter</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Instagram</li>\n          </ul>\n        </div>\n        <div class=\"col-md-4 col-xs-0\"></div>\n        <div class=\"col-md-4 col-xs-12\">\n          <img class=\"img-responsive\" src=\"assets/images/wt.png\" alt=\"\">\n        </div>\n      </div>\n      <div>\n        <hr style=\"color: #9c9ca0\">\n      </div>\n    </div>\n    <div class=\"row\" style=\"margin-top: -1%;\">\n      <div class=\"col-md-4 col-xs-4\"></div>\n      <div class=\"col-md-4 col-xs-4\" style=\"display: inline-flex;\">\n        <span style=\"margin-right: 15%; margin-left: 25%;\"><img class=\"img-responsive\" width=\"30px\" height=\"30px\" src=\"assets/icons/facebook.png\" alt=\"\"></span>\n        <span style=\"margin-right: 15%;\"><img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/tweet.png\" alt=\"\"></span>\n        <span style=\"margin-right: 15%;\"><img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/insta.png\" alt=\"\"></span>\n      </div>\n      <div class=\"col-md-4 col-xs-4\"></div>\n    </div>\n    <div style=\"text-align: center; font-family: Roboto; color: #9c9ca0; margin-top: 5%;\">\n      &copy; <span>ticketMax, 2018</span>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomeComponent = (function () {
    // userName: string;
    function HomeComponent(dataService, route, router, spinnerService) {
        var _this = this;
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.spinnerService.show();
        this.user = localStorage.getItem('token');
        this.userType = localStorage.getItem('userId');
        // console.log(localStorage);
        dataService.getEvents().subscribe(function (events) {
            _this.events = events;
            // tslint:disable-next-line:forin
            for (var key in _this.events) {
                _this.process_str = _this.events[key]['image_url'];
                _this.events[key]['ref_image_url'] = _this.process_str.split('.jpg')[0] + 'm.jpg';
            }
            _this.spinnerService.hide();
            // console.log(this.events);
        });
        // console.log(this.userName);
        if (this.userType === 'user') {
            this.userToggle = false;
        }
        else {
            this.userToggle = true;
        }
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.sendData = function (id, image, date, name, loc) {
        this.dataService.sendEvent(id, image, date, name, loc);
    };
    HomeComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/']);
        window.location.reload();
    };
    HomeComponent.prototype.gotoUser = function () {
        window.location.href = '/user-login';
    };
    HomeComponent.prototype.gotoOrg = function () {
        window.location.replace('/org-login');
    };
    HomeComponent.prototype.moveAway = function () {
        this.router.navigate(['/browse_events']);
    };
    HomeComponent.prototype.reRoute = function (id) {
        this.router.navigate(['/ticket', id]);
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/components/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/list-tickets/list-tickets.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n.nav>li>a:focus,\r\n.nav>li>a:hover {\r\n  background-color: #24242e;\r\n  color: white;\r\n}\r\n\r\n.btn.focus,\r\n.btn:focus,\r\n.btn:hover {\r\n  color: white;\r\n}\r\n\r\n.foot {\r\n  position: fixed;\r\n  left: 0;\r\n  bottom: 0;\r\n  width: 100%;\r\n  height: 25%;\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  #fit {\r\n    margin-top: 20%;\r\n  }\r\n}\r\n\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/list-tickets/list-tickets.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar-fixed></app-navbar-fixed>\n\n<div id=\"fit\">\n  <h2 style=\"font-family: Roboto; color: #24242e; font-weight: 700; text-align: center; margin-top: 7%;\">{{name}}</h2>\n  <h4 style=\"font-family: Roboto; color: #24242e; font-weight: 700; text-align: center; margin-top: 3%;\">Tickets</h4>\n  <hr style=\"margin-bottom: 0%;\">\n  <div style=\"background-color: #f8f8fa; padding-top: 1%;\">\n    <div class=\"container-fluid\">\n      <div ng-if=\"list_ticket\" class=\"row\" style=\"text-align: center;\">\n        <div *ngFor=\"let item of list_ticket\" class=\"col-md-4 col-xs-12\" style=\"margin-bottom: 5%;\">\n          <div style=\"width: 300px; margin: auto;\">\n            <div style=\"background-color: white; padding: 2% 3% 4% 3%;\">\n              <h4 style=\"color: #24242e; font-weight: 700; text-align: left; margin-bottom: 1%;\">{{item.event.name}}</h4>\n              <p style=\"color: #b6b6b6; font-weight: 700; text-align: left; font-size: 1; margin: 0%;\">{{item.event.location}}</p>\n              <p style=\"color: #b6b6b6; font-weight: 700; text-align: left; font-size: 1; margin: 0%;\">{{item.event.date}}</p>\n              <p style=\"color: #b6b6b6; font-weight: 700; text-align: left; font-size: 1; margin: 0%;\">{{item.event.time}}</p>\n            </div>\n            <img style=\"margin-top: -3%\" width=\"20px\" height=\"150px\" width=\"300px\" src=\"{{item.ref_image_url}}\" alt=\"\">\n          </div>\n        </div>\n\n      </div>\n    </div>\n  </div>\n</div>\n\n<!-- Beginning Of Footer -->\n<div style=\"margin-top: 100px; background-color: #24242e;\" class=\"foot\">\n  <div class=\"container-fluid\" style=\"font-family: Roboto;\">\n    <div class=\"container\">\n      <div class=\"row\" style=\"margin-top: 2%;\">\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">Gatepass</li>\n            <a [routerLink]=\"[ '/home' ]\">\n              <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Home</li>\n            </a>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Events</li>\n          </ul>\n        </div>\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">Contact</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">FAQ</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Contact Us</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Facebook</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Twitter</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Instagram</li>\n          </ul>\n        </div>\n        <div class=\"col-md-4 col-xs-0\"></div>\n        <div class=\"col-md-4 col-xs-12\">\n          <img class=\"img-responsive\" src=\"assets/images/wt.png\" alt=\"\">\n        </div>\n      </div>\n      <div>\n        <hr style=\"color: #9c9ca0\">\n      </div>\n    </div>\n    <div class=\"row\" style=\"margin-top: -1%;\">\n      <div class=\"col-md-4 col-xs-4\"></div>\n      <div class=\"col-md-4 col-xs-4\" style=\"display: inline-flex;\">\n        <span style=\"margin-right: 15%; margin-left: 25%;\"><img class=\"img-responsive\" width=\"30px\" height=\"30px\" src=\"assets/icons/facebook.png\" alt=\"\"></span>\n        <span style=\"margin-right: 15%;\"><img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/tweet.png\" alt=\"\"></span>\n        <span style=\"margin-right: 15%;\"><img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/insta.png\" alt=\"\"></span>\n      </div>\n      <div class=\"col-md-4 col-xs-4\"></div>\n    </div>\n    <div style=\"text-align: center; font-family: Roboto; color: #9c9ca0; margin-top: 1%;\">\n      &copy; <span>Gatepass, 2018</span>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/list-tickets/list-tickets.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListTicketsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListTicketsComponent = (function () {
    function ListTicketsComponent(dataService, spinnerService, route, router) {
        var _this = this;
        this.dataService = dataService;
        this.spinnerService = spinnerService;
        this.route = route;
        this.router = router;
        this.spinnerService.show();
        this.name = localStorage.getItem('name');
        this.dataService.getTicketLists().subscribe(function (res) {
            _this.list_ticket = res;
            // tslint:disable-next-line:forin
            for (var key in _this.list_ticket) {
                _this.process_str = _this.list_ticket[key]['event']['image_url'];
                _this.list_ticket[key]['ref_image_url'] = _this.process_str.split('.jpg')[0] + 'm.jpg';
                // console.log(this.list_ticket[key]['ref_image_url']);
            }
            _this.spinnerService.hide();
        });
    }
    ListTicketsComponent.prototype.ngOnInit = function () {
    };
    ListTicketsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-list-tickets',
            template: __webpack_require__("../../../../../src/app/components/list-tickets/list-tickets.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/list-tickets/list-tickets.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2_ng4_loading_spinner__["Ng4LoadingSpinnerService"], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */]])
    ], ListTicketsComponent);
    return ListTicketsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login-org/login-org.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media only screen and (min-width: 600px) {\r\n  #fit {\r\n    margin-left: -20px;\r\n  }\r\n}\r\n\r\n.anim {\r\n  position: relative;\r\n  -webkit-animation-name: example;\r\n  /* Chrome, Safari, Opera */\r\n  -webkit-animation-duration: 4s;\r\n  /* Chrome, Safari, Opera */\r\n  animation-name: example;\r\n  animation-duration: 4s;\r\n}\r\n\r\n/* Chrome, Safari, Opera */\r\n\r\n/* Standard syntax */\r\n\r\n@keyframes example {\r\n  0% {\r\n    left: 200px;\r\n    top: 0px;\r\n  }\r\n  50% {\r\n    left: 0px;\r\n    top: 0px;\r\n  }\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login-org/login-org.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-5 col-xs-12\" style=\"padding: 5% 5% 0% 5%; margin-bottom: 10%;\">\n    <div style=\"margin-left: 17%\">\n      <a [routerLink]=\"[ '/' ]\">\n        <img width=\"80%\" height=\"80%\" class=\"img-responsive\" src=\"assets/images/transparent.PNG\" alt=\"\">\n      </a>\n    </div>\n    <div *ngIf=\"showLogin\" [ngClass]=\"{'anim': showStyle}\">\n      <app-register-org></app-register-org>\n    </div>\n    <div *ngIf=\"!showLogin\" [ngClass]=\"{'anim': showStyle}\">\n      <app-sign-org></app-sign-org>\n    </div>\n  </div>\n\n  <div class=\"col-md-7 col-xs-12\" style=\"background-color: #ef4a4e; padding: 1% 10% 0% 10%;\">\n    <div class=\"row\">\n      <div class=\"col-md-6 col-xs-0\">.</div>\n      <div style=\"display: inline-flex;\" class=\"col-md-2 col-xs-4\">\n        <button class=\"btn btn-md\" style=\"background-color: white; color: #ef4a4e; font-weight: 700\">Download App</button>\n      </div>\n      <div style=\"display: inline-flex; margin-left: 10%;\" class=\"col-md-2 col-xs-4\">\n        <button class=\"btn btn-md\" (click)=\"reveal()\" style=\"background-color: #ef4a4e; color: white; border: 1px solid white;\n         font-weight: 700; padding: 7% 50%;\">\n          <div *ngIf=\"!showLogin\">\n            Sign Up\n          </div>\n          <div *ngIf=\"showLogin\">\n            Log In\n          </div>\n        </button>\n      </div>\n    </div>\n    <div style=\"margin-top: 15%; font-family: Roboto; color: white; text-align: center\">\n      <h2 style=\"font-weight: 700; margin: 3px;\">Get a great insight of your</h2>\n      <h2 style=\"font-weight: 700; margin: 3px;\">tickets and have total control</h2>\n      <h2 style=\"font-weight: 700; margin: 3px;\">over your tickets</h2>\n      <p style=\"margin-top: 20%; font-weight: 200; margin-bottom: 5%;\">Just a single click away</p>\n      <div style=\"padding: 0% 20% 0% 20%;\">\n        <img class=\"img-responsive\" src=\"assets/images/orgphonelogin.png\" alt=\"\">\n      </div>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/login-org/login-org.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginOrgComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginOrgComponent = (function () {
    function LoginOrgComponent(dataService) {
        this.dataService = dataService;
        this.styleToggle = 1;
        this.showStyle = false;
        this.showStyling = true;
    }
    LoginOrgComponent.prototype.ngOnInit = function () {
    };
    LoginOrgComponent.prototype.reveal = function () {
        this.showLogin = !this.showLogin;
        if (this.styleToggle = 1) {
            this.showStyle = true;
            this.showStyling = false;
            ++this.styleToggle;
        }
        else {
            this.showStyle = !this.showStyle;
            this.showStyling = !this.showStyling;
        }
    };
    LoginOrgComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login-org',
            template: __webpack_require__("../../../../../src/app/components/login-org/login-org.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login-org/login-org.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */]])
    ], LoginOrgComponent);
    return LoginOrgComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login-user/login-user.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media only screen and (min-width: 600px) {\r\n  #fit {\r\n    margin-left: -20px;\r\n  }\r\n}\r\n\r\n.anim {\r\n  position: relative;\r\n  -webkit-animation-name: example;\r\n  /* Chrome, Safari, Opera */\r\n  -webkit-animation-duration: 4s;\r\n  /* Chrome, Safari, Opera */\r\n  animation-name: example;\r\n  animation-duration: 4s;\r\n}\r\n\r\n/* Chrome, Safari, Opera */\r\n\r\n\r\n/* Standard syntax */\r\n\r\n@keyframes example {\r\n  0% {\r\n    left: 200px;\r\n    top: 0px;\r\n  }\r\n\r\n  50% {\r\n    left: 0px;\r\n    top: 0px;\r\n  }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login-user/login-user.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-5 col-xs-12\" style=\"padding: 5% 5% 0% 5%; margin-bottom: 10%;\">\n    <div style=\"margin-left: 17%\">\n      <a [routerLink]=\"[ '/' ]\">\n        <img width=\"80%\" height=\"80%\" class=\"img-responsive\" src=\"assets/images/transparent.PNG\" alt=\"\">\n      </a>\n    </div>\n    <div *ngIf=\"showLogin\" [ngClass]=\"{'anim': showStyle}\">\n      <app-register-user></app-register-user>\n    </div>\n    <div *ngIf=\"!showLogin\" [ngClass]=\"{'anim': showStyle}\">\n      <app-sign-user></app-sign-user>\n    </div>\n  </div>\n\n  <div class=\"col-md-7 col-xs-12\" style=\"background-color: #24242e; padding: 1% 10% 0% 10%;\">\n    <div class=\"row\">\n      <div class=\"col-md-6 col-xs-0\">.</div>\n      <div style=\"display: inline-flex;\" class=\"col-md-2 col-xs-4\">\n        <button class=\"btn btn-md\" style=\"background-color: white; color:#24242e; font-weight: 700\">Download App</button>\n      </div>\n      <div style=\"display: inline-flex; margin-left: 10%;\" class=\"col-md-2 col-xs-4\">\n        <button class=\"btn btn-md\" (click)=\"reveal()\" style=\"background-color:#24242e; color: white; border: 1px solid white;\n         font-weight: 700; padding: 7% 50%;\">\n         <div *ngIf=\"!showLogin\">\n           Sign Up\n         </div>\n         <div *ngIf=\"showLogin\">\n           Log In\n         </div>\n        </button>\n      </div>\n    </div>\n    <div style=\"margin-top: 15%; font-family: Roboto; color: white; text-align: center\">\n      <h2 style=\"font-weight: 700; margin: 3px;\">Save your tickets for offline</h2>\n      <h2 style=\"font-weight: 700; margin: 3px;\">use and share with your</h2>\n      <h2 style=\"font-weight: 700; margin: 3px;\">friends</h2>\n      <p style=\"margin-top: 20%; font-weight: 200; margin-bottom: 5%;\">Just a single click away</p>\n      <div style=\"padding: 0% 20% 0% 20%;\">\n        <img class=\"img-responsive\" src=\"assets/images/phoneR.png\" alt=\"\">\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/login-user/login-user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginUserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginUserComponent = (function () {
    function LoginUserComponent(dataService) {
        this.dataService = dataService;
        this.styleToggle = 1;
        this.showStyle = false;
        this.showStyling = true;
    }
    LoginUserComponent.prototype.ngOnInit = function () {
    };
    LoginUserComponent.prototype.reveal = function () {
        this.showLogin = !this.showLogin;
        if (this.styleToggle = 1) {
            this.showStyle = true;
            this.showStyling = false;
            ++this.styleToggle;
        }
        else {
            this.showStyle = !this.showStyle;
            this.showStyling = !this.showStyling;
        }
    };
    LoginUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login-user',
            template: __webpack_require__("../../../../../src/app/components/login-user/login-user.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login-user/login-user.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */]])
    ], LoginUserComponent);
    return LoginUserComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar-fixed/navbar-fixed.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#menu-img {\r\n  margin-top: 21% !important;\r\n  margin-left: 3% !important;\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #menu-img {\r\n    margin-top: 5% !important;\r\n    margin-left: 3% !important;\r\n  }\r\n}\r\n\r\n\r\n@media screen and (max-width:600px) {\r\n  #adjust {\r\n    margin-top: 0% !important;\r\n    margin-left: 1% !important;\r\n  }\r\n}\r\n\r\n.dropBtn {\r\n  background-color: #24242e !important;\r\n  color: white;\r\n  background-image: none !important;\r\n  border: none;\r\n  border-radius: 8px;\r\n}\r\n\r\n.btn-default.active.focus,\r\n.btn-default.active:focus,\r\n.btn-default.active:hover,\r\n.btn-default:active.focus,\r\n.btn-default:active:focus,\r\n.btn-default:active:hover,\r\n.open>.dropdown-toggle.btn-default.focus,\r\n.open>.dropdown-toggle.btn-default:focus,\r\n.open>.dropdown-toggle.btn-default:hover {\r\n  color: white !important;\r\n  background-color: transparent !important;\r\n  border-color: none !important;\r\n}\r\n\r\n@media (max-width: 767px) {\r\n  .navbar-nav .open .dropdown-menu {\r\n    background-color: white !important;\r\n  }\r\n}\r\n\r\n.nav>li>a:focus,\r\n.nav>li>a:hover {\r\n  background-color: #24242e;\r\n  color: white;\r\n}\r\n\r\n.btn.focus,\r\n.btn:focus,\r\n.btn:hover {\r\n  color: white;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar-fixed/navbar-fixed.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-fixed-top\" style=\"background-color: #24242e\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" style=\"border-color: #9c9ca0\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n        <img src=\"assets/icons/menu.png\" width=\"20px\" height=\"20px\" alt=\"\">\n      </button>\n      <a [routerLink]=\"[ '/' ]\">\n        <img id=\"menu-img\" style=\"margin-top: 21%; margin-left: 10px;\" class=\"img-responsive\" src=\"assets/images/tmsm.png\">\n      </a>\n    </div>\n    <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li>\n          <a [routerLink]=\"[ '/browse_events' ]\" style=\"color: white; font-weight: 600\">Browse Events</a>\n        </li>\n        <li>\n          <a [routerLink]=\"[ '/pricing' ]\" style=\"color: white; font-weight: 600\">Pricing</a>\n        </li>\n        <li *ngIf=\"user && !userToggle\">\n          <a [routerLink]=\"[ '/tickets' ]\" style=\"color: white; font-weight: 600\">Tickets</a>\n        </li>\n        <li>\n          <a *ngIf=\"!user\" [routerLink]=\"[ '/block' ]\" style=\"color: white;\">\n            <button class=\"btn btn-sm up-button\" style=\"margin-top: -2px; border-radius: 8px; background-color: #ef4a4e;\">Create Events</button>\n          </a>\n        </li>\n        <li>\n          <a *ngIf=\"userToggle && user\" [routerLink]=\"[ '/dashboard' ]\" style=\"color: white;\">\n            <button class=\"btn btn-sm up-button\" style=\"margin-top: -2px; border-radius: 8px; background-color: #ef4a4e;\">Dashboard</button>\n          </a>\n        </li>\n        <li *ngIf=\"!user\">\n          <div id=\"adjust\" class=\"dropdown\" style=\"margin-top: 13%;\">\n            <button class=\"btn btn-default dropdown-toggle dropBtn\" type=\"button\" id=\"login\" data-toggle=\"dropdown\">Login\n              <span class=\"caret\"></span>\n            </button>\n            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"login\">\n              <li role=\"presentation\">\n                <a role=\"menuitem\" [routerLink]=\"[ '/user-login' ]\">Login As A User</a>\n              </li>\n              <li class=\"divider\"></li>\n              <li role=\"presentation\">\n                <a role=\"menuitem\" [routerLink]=\"[ '/org-login' ]\">Login As An Organiser</a>\n              </li>\n            </ul>\n          </div>\n        </li>\n        <li *ngIf=\"user\" style=\"cursor: pointer\">\n          <a (click)=\"logout()\" style=\"color: white; font-weight: 600\">Logout</a>\n        </li>\n      </ul>\n    </div>\n  </div>\n</nav>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/navbar-fixed/navbar-fixed.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarFixedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarFixedComponent = (function () {
    function NavbarFixedComponent(dataService, router, route) {
        this.dataService = dataService;
        this.router = router;
        this.route = route;
        this.user = localStorage.getItem('token');
        this.userType = localStorage.getItem('userId');
        console.log(this.userType);
        if (this.userType === 'user') {
            this.userToggle = false;
        }
        else {
            this.userToggle = true;
        }
    }
    NavbarFixedComponent.prototype.ngOnInit = function () {
    };
    NavbarFixedComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/']);
    };
    NavbarFixedComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar-fixed',
            template: __webpack_require__("../../../../../src/app/components/navbar-fixed/navbar-fixed.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar-fixed/navbar-fixed.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], NavbarFixedComponent);
    return NavbarFixedComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#menu-img {\r\n  margin-top: 21%;\r\n  margin-left: 3%;\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #menu-img {\r\n    margin-top: 5%;\r\n    margin-left: 3%;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #adjust {\r\n    margin-top: 0% !important;\r\n    margin-left: 1% !important;\r\n  }\r\n}\r\n\r\n.dropBtn {\r\n  background-color: #24242e !important;\r\n  color: white;\r\n  background-image: none !important;\r\n  border: none;\r\n  border-radius: 8px;\r\n}\r\n\r\n.btn-default.active.focus,\r\n.btn-default.active:focus,\r\n.btn-default.active:hover,\r\n.btn-default:active.focus,\r\n.btn-default:active:focus,\r\n.btn-default:active:hover,\r\n.open>.dropdown-toggle.btn-default.focus,\r\n.open>.dropdown-toggle.btn-default:focus,\r\n.open>.dropdown-toggle.btn-default:hover {\r\n  color: white !important;\r\n  background-color: transparent !important;\r\n  border-color: none !important;\r\n}\r\n\r\n@media (max-width: 767px) {\r\n  .navbar-nav .open .dropdown-menu {\r\n    background-color: white !important;\r\n  }\r\n}\r\n\r\n.nav>li>a:focus,\r\n.nav>li>a:hover {\r\n  background-color: #24242e;\r\n  color: white;\r\n}\r\n\r\n.btn.focus,\r\n.btn:focus,\r\n.btn:hover {\r\n  color: white;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar\" style=\"background-color: #24242e\">\n  <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" style=\"border-color: #9c9ca0\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n        <img src=\"assets/icons/menu.png\" width=\"20px\" height=\"20px\" alt=\"\">\n      </button>\n      <a [routerLink]=\"[ '/' ]\">\n        <img id=\"menu-img\" class=\"img-responsive\" src=\"assets/images/tmsm.png\">\n      </a>\n    </div>\n    <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li>\n          <a [routerLink]=\"[ '/browse_events' ]\" style=\"color: white; font-weight: 600\">Browse Events</a>\n        </li>\n        <li>\n          <a [routerLink]=\"[ '/pricing' ]\" style=\"color: white; font-weight: 600\">Pricing</a>\n        </li>\n        <li *ngIf=\"user && !userToggle\">\n          <a [routerLink]=\"[ '/tickets' ]\" style=\"color: white; font-weight: 600\">Tickets</a>\n        </li>\n        <li>\n          <a *ngIf=\"!user\" [routerLink]=\"[ '/block' ]\" style=\"color: white;\">\n            <button class=\"btn btn-sm up-button\" style=\"margin-top: -2px; border-radius: 8px; background-color: #ef4a4e;\">Create Events</button>\n          </a>\n        </li>\n        <li>\n          <a *ngIf=\"userToggle && user\" [routerLink]=\"[ '/dashboard' ]\" style=\"color: white;\">\n            <button class=\"btn btn-sm up-button\" style=\"margin-top: -2px; border-radius: 8px; background-color: #ef4a4e;\">Dashboard</button>\n          </a>\n        </li>\n        <li *ngIf=\"!user\">\n          <div id=\"adjust\" class=\"dropdown\" style=\"margin-top: 13%;\">\n            <button class=\"btn btn-default dropdown-toggle dropBtn\" type=\"button\" id=\"login\" data-toggle=\"dropdown\">Login\n              <span class=\"caret\"></span>\n            </button>\n            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"login\">\n              <li role=\"presentation\">\n                <a role=\"menuitem\" [routerLink]=\"[ '/user-login' ]\">Login As A User</a>\n              </li>\n              <li class=\"divider\"></li>\n              <li role=\"presentation\">\n                <a role=\"menuitem\" [routerLink]=\"[ '/org-login' ]\">Login As An Organiser</a>\n              </li>\n            </ul>\n          </div>\n        </li>\n        <li *ngIf=\"user\" style=\"cursor: pointer\">\n          <a (click)=\"logout()\" style=\"color: white; font-weight: 600\">Logout</a>\n        </li>\n      </ul>\n    </div>\n  </div>\n</nav>\n"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarComponent = (function () {
    function NavbarComponent(dataService, router, route) {
        this.dataService = dataService;
        this.router = router;
        this.route = route;
        this.user = localStorage.getItem('token');
        this.userType = localStorage.getItem('userId');
        console.log(this.userType);
        if (this.userType === 'user') {
            this.userToggle = false;
        }
        else {
            this.userToggle = true;
        }
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/']);
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/pricing/pricing.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width:600px) {\r\n  #menu_img {\r\n    margin-top: 2%;\r\n    margin-left: 3%;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #menu_img {\r\n    margin-top: 8%;\r\n    margin-left: 3%;\r\n  }\r\n}\r\n\r\n@media screen and (max-width:600px) {\r\n  #adjust {\r\n    margin-top: 0% !important;\r\n    margin-left: 1% !important;\r\n  }\r\n}\r\n\r\n.dropBtn {\r\n  background-color: #24242e !important;\r\n  color: white;\r\n  background-image: none !important;\r\n  border: none;\r\n  border-radius: 8px;\r\n}\r\n\r\n.btn-default.active.focus,\r\n.btn-default.active:focus,\r\n.btn-default.active:hover,\r\n.btn-default:active.focus,\r\n.btn-default:active:focus,\r\n.btn-default:active:hover,\r\n.open>.dropdown-toggle.btn-default.focus,\r\n.open>.dropdown-toggle.btn-default:focus,\r\n.open>.dropdown-toggle.btn-default:hover {\r\n  color: white !important;\r\n  background-color: transparent !important;\r\n  border-color: none !important;\r\n}\r\n\r\n@media (max-width: 767px){\r\n  .navbar-nav .open .dropdown-menu {\r\n    background-color: white !important;\r\n  }\r\n}\r\n\r\n\r\n.nav>li>a:focus,\r\n.nav>li>a:hover {\r\n  background-color: #24242e;\r\n  color: white;\r\n}\r\n\r\n.btn.focus,\r\n.btn:focus,\r\n.btn:hover {\r\n  color: white;\r\n}\r\n\r\n.contains {\r\n  width: 280px;\r\n  height: 415px;\r\n}\r\n\r\n.spacing {\r\n  padding-top: 18%;\r\n  padding-right: 2%;\r\n  padding-left: 2%;\r\n  padding-bottom: 6%;\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  #fit-in {\r\n    margin-top: 5%;\r\n    margin-left: 8%;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  #styl {\r\n    display: block;\r\n    margin-top: 1%;\r\n  }\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/pricing/pricing.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<!-- End of Nav -->\n<div style=\"margin-top: 3%\">\n  <h3 style=\"text-align: center; font-family: Roboto; font-weight: 600; color: #24242e;\">Lowest pricing, best services</h3>\n</div>\n<!-- End of heading text -->\n<div class=\"container\" style=\"margin-top: 7%; text-align: center;\">\n  <div class=\"row\">\n    <div class=\"col-md-2 col-xs-1\"></div>\n    <div class=\"col-md-4 col-xs-8\">\n      <div class=\"contains\">\n        <div style=\"background-image: url('assets/images/pricing1.png'); background-repeat: no-repeat; object-fit: contain\">\n          <div class=\"spacing\" style=\"color: white; text-align: center; font-family: Roboto;\">\n            <h4>Gatepass</h4>\n            <p style=\"font-size: 700; margin-top: 10%;\">We charge 10% of the</p>\n            <p style=\"font-size: 700; margin-top: -3%;\">total sales</p>\n            <p style=\"margin-top: 10%; font-weight: 100;\">We charge the lowest rates in the market and free events are 100% FREE</p>\n            <div style=\"margin-top: 8%; padding-left: 10%; padding-right: 10%;\">\n              <hr style=\"color: #9c9ca0\">\n            </div>\n            <p style=\"font-size: 700;\">Organizer Dashboard</p>\n            <p style=\"font-size: 700;\">24/7 Support</p>\n            <p style=\"font-size: 700;\">Fast Cash Out</p>\n            <div style=\"margin-top: 10%; margin-bottom: 2%;\">\n              <a href=\"\" style=\"color: white;\"><button class=\"btn btn-sm\" style=\"border-radius: 8px; background-color: #ef4a4e; box-shadow: 2px 2px 8px 0px #ef4a4e;\">Choose Plan</button></a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div id=\"fit-in\" class=\"col-md-4 col-xs-8\">\n      <div class=\"contains\">\n        <div style=\"background-image: url('assets/images/pricing2.png'); background-repeat: no-repeat; object-fit: contain\">\n          <div class=\"spacing\" style=\"color: white; text-align: center; font-family: Roboto;\">\n            <h4 style=\"margin-top: 20%;\">The Other Guys</h4>\n            <p style=\"font-size: 17px; margin-top: 15%; font-weight: 700;\">20 - 25%</p>\n            <div style=\"margin-top: 20%; padding-left: 10%; padding-right: 10%;\">\n              <hr style=\"background-color: #24242e; height: 1px; border: 0;\">\n            </div>\n            <p style=\"font-size: 700;\">No Organizer Dashboard</p>\n            <p style=\"font-size: 700;\">No Support</p>\n            <p style=\"font-size: 700; margin-bottom: 20%\">Weekly Cash Out</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<!-- Ending Of 2 Boxes -->\n<div style=\"text-align: center; margin-top: 5%; margin-left: -5%; color: #24242e; font-family: Roboto;\">\n  <h5 style=\"font-weight: 700; font-size: 17px;\">Calculate Your Fees</h5>\n  <img src=\"assets/icons/Cash.png\" alt=\"\" style=\"margin-top: 1%;\">\n  <div>\n    <span style=\"font-weight: 700;\">\n      <label for=\"ticket_price\">Ticket Price: </label>\n      <span style=\"display: inline-flex; margin-top: 1%\" class=\"form-group\">\n        <input id=\"styl\" style=\"margin-left: 46%;\" class=\"form-control\" placeholder=\"₦\" [(ngModel)]=\"ticket_price\" type=\"text\" name=\"ticket_price\" id=\"ticket_price\">\n      </span>\n    </span>\n  </div>\n  <div>\n    <span style=\"font-weight: 700;\" class=\"row\">\n      <label id=\"styl\" for=\"expect_buyers\">Expected number of buyers:</label>\n      <div id=\"styl\" style=\"display: inline-flex; margin-top: 1%;\" class=\"form-group\">\n        <input class=\"form-control\" style=\"margin-left: 3%;\" placeholder=\"₦\" [(ngModel)]=\"expect_buyers\" type=\"text\" name=\"expect_buyers\" id=\"expect_buyers\">\n      </div>\n    </span>\n  </div>\n  <div id=\"rule\" style=\"padding-left: 38%; padding-right: 38%;\">\n    <hr style=\"background-color: #24242e; height: 1px; border: 0;\">\n  </div>\n  <div>\n    <span style=\"font-weight: 700;\">\n       Your Buyers Pay: <span style=\"margin-left: 7%\">₦ {{ ticket_price * expect_buyers }}</span>\n    </span>\n  </div>\n  <div style=\"margin-top: 0.5%;\">\n    <span style=\"font-weight: 700;\">\n       You Receive: <span style=\"margin-left: 8.5%;\">₦ {{ (ticket_price * expect_buyers) - (ticket_price * expect_buyers) * 0.1 }}</span>\n    </span>\n  </div>\n</div>\n<!-- Beginning Of Footer -->\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/pricing/pricing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PricingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PricingComponent = (function () {
    function PricingComponent() {
        this.ticket_price = 0;
        this.expect_buyers = 0;
    }
    PricingComponent.prototype.ngOnInit = function () {
    };
    PricingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-pricing',
            template: __webpack_require__("../../../../../src/app/components/pricing/pricing.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/pricing/pricing.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PricingComponent);
    return PricingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/register-org/register-org.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".notAuth {\r\n  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(219, 58, 9, 0.781)\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register-org/register-org.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"margin-top: 5%;\">\n  <h2 style=\"font-family: Roboto; color: #24242e; text-align: center; font-weight: 700;\">Sign up</h2>\n</div>\n<div style=\"margin-top: 10%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"text\" class=\"form-control\" id=\"fullname\" name=\"fullname\" [(ngModel)]=\"fullname\" placeholder=\"Fullname\">\n  </div>\n</div>\n<div style=\"margin-top: 3%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"text\" class=\"form-control\" id=\"username\" name=\"username\"\n      [(ngModel)]=\"username\" placeholder=\"Username\">\n  </div>\n</div>\n<div style=\"margin-top: 6%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"email\" placeholder=\"Email Address\">\n  </div>\n</div>\n<div style=\"margin-top: 6%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=\"password\"\n      placeholder=\"Password\">\n  </div>\n</div>\n<div style=\"margin-top: 6%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"tel\" class=\"form-control\" id=\"phone_number\" name=\"phone_number\" [(ngModel)]=\"phone_number\"\n      placeholder=\"Phone Number\">\n  </div>\n</div>\n<!-- <div style=\"float: right; padding-right: 15%\">\n  <a [routerLink]=\"[ '/login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Already Signed Up?</p>\n  </a>\n</div> -->\n<div style=\"margin-top: 12%; padding: 0% 15% 0% 15%;\">\n  <button type=\"submit\" class=\"btn btn-lg\" (click)=sendData() style=\"width: 100%; border-radius: 0px; background-color: #24242e; color:white\">Sign Up</button>\n</div>\n<div *ngIf=\"notAuth\">\n  <p style=\"font-family: Roboto; font-weight: 900; color: #ef4a4e; margin-left: 15%;\">Incorrect Details</p>\n</div>\n<div style=\"float: right; padding-right: 15%; margin-top: 3%;\">\n  <a [routerLink]=\"[ '/user-login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Are You A User?</p>\n  </a>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/register-org/register-org.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterOrgComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterOrgComponent = (function () {
    function RegisterOrgComponent(dataService, route, router, spinnerService) {
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.notAuth = false;
        localStorage.clear();
    }
    RegisterOrgComponent.prototype.ngOnInit = function () {
    };
    RegisterOrgComponent.prototype.sendData = function () {
        var _this = this;
        this.spinnerService.show();
        var obj = {
            fullname: this.fullname,
            username: this.username,
            email: this.email,
            password: this.password,
            phone: this.phone_number,
        };
        this.dataService.registerOrgs(obj).subscribe(function (response) {
            if (response.status === true) {
                _this.user = JSON.parse(localStorage.getItem('token'));
                localStorage.setItem('token', JSON.stringify(response.token));
                localStorage.setItem('userId', 'org');
                localStorage.setItem('name', response.user.fullname);
                localStorage.setItem('email', response.user.email);
                _this.spinnerService.hide();
                window.location.replace('http://www.ticketmax.ng/dashboard');
            }
            else {
                _this.spinnerService.hide();
                _this.notAuth = true;
            }
        }, function (error) {
            console.log(error);
            _this.spinnerService.hide();
            _this.notAuth = true;
        });
    };
    RegisterOrgComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register-org',
            template: __webpack_require__("../../../../../src/app/components/register-org/register-org.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register-org/register-org.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], RegisterOrgComponent);
    return RegisterOrgComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/register-user/register-user.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".notAuth {\r\n    box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(219, 58, 9, 0.781)\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register-user/register-user.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"margin-top: 5%;\">\n  <h2 style=\"font-family: Roboto; color: #24242e; text-align: center; font-weight: 700;\">Sign up</h2>\n</div>\n<div style=\"margin-top: 10%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"text\" class=\"form-control\" id=\"fullname\" name=\"fullname\" [(ngModel)]=\"fullname\" placeholder=\"Fullname\">\n  </div>\n</div>\n<div style=\"margin-top: 3%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"email\" placeholder=\"Email Address\">\n  </div>\n</div>\n<div style=\"margin-top: 6%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=\"password\"\n      placeholder=\"Password\">\n  </div>\n</div>\n<div style=\"margin-top: 6%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"tel\" class=\"form-control\" id=\"phone_number\" name=\"phone_number\" [(ngModel)]=\"phone_number\"\n      placeholder=\"Phone Number\">\n  </div>\n</div>\n<!-- <div style=\"float: right; padding-right: 15%\">\n  <a [routerLink]=\"[ '/login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Already Signed Up?</p>\n  </a>\n</div> -->\n<div style=\"margin-top: 12%; padding: 0% 15% 0% 15%;\">\n  <button type=\"submit\" class=\"btn btn-lg\" (click)=sendData() style=\"width: 100%; border-radius: 0px; background-color: #24242e; color:white\">Sign Up</button>\n</div>\n<div *ngIf=\"notAuth\">\n  <p style=\"font-family: Roboto; font-weight: 900; color: #ef4a4e; margin-left: 15%;\">Incorrect Details</p>\n</div>\n<div style=\"float: right; padding-right: 15%; margin-top: 3%;\">\n  <a [routerLink]=\"[ '/org-login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Are You An Organiser?</p>\n  </a>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/register-user/register-user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterUserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterUserComponent = (function () {
    function RegisterUserComponent(dataService, route, router, spinnerService) {
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.sources = 'EM';
        this.notAuth = false;
        localStorage.clear();
    }
    RegisterUserComponent.prototype.ngOnInit = function () {
    };
    RegisterUserComponent.prototype.sendData = function () {
        var _this = this;
        this.spinnerService.show();
        var obj = {
            fullname: this.fullname,
            email: this.email,
            password: this.password,
            phone: this.phone_number,
            source: this.sources
        };
        this.dataService.registerUsers(obj).subscribe(function (response) {
            if (response.status === true) {
                localStorage.setItem('userId', 'user');
                localStorage.setItem('token', JSON.stringify(response.token));
                _this.user = JSON.parse(localStorage.getItem('token'));
                localStorage.setItem('name', response.user.fullname);
                localStorage.setItem('email', response.user.email);
                _this.spinnerService.hide();
                window.location.replace('http://www.ticketmax.ng/');
            }
            else {
                _this.spinnerService.hide();
                _this.notAuth = true;
            }
        }, function (error) {
            console.log('There was an error');
            _this.spinnerService.hide();
            _this.notAuth = true;
        });
    };
    RegisterUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register-user',
            template: __webpack_require__("../../../../../src/app/components/register-user/register-user.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register-user/register-user.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], RegisterUserComponent);
    return RegisterUserComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/sidenav/sidenav.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/sidenav/sidenav.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  sidenav works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/sidenav/sidenav.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidenavComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidenavComponent = (function () {
    function SidenavComponent() {
    }
    SidenavComponent.prototype.ngOnInit = function () {
    };
    SidenavComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sidenav',
            template: __webpack_require__("../../../../../src/app/components/sidenav/sidenav.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/sidenav/sidenav.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidenavComponent);
    return SidenavComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/sign-org/sign-org.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".notAuth {\r\n    box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(219, 58, 9, 0.781)\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/sign-org/sign-org.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"margin-top: 2%;\">\n  <h2 style=\"font-family: Roboto; color: #24242e; text-align: center; font-weight: 700;\">Sign in to your account</h2>\n</div>\n<div style=\"margin-top: 10%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"email\" placeholder=\"Email Address\">\n  </div>\n</div>\n<div style=\"margin-top: 6%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=\"password\"\n      placeholder=\"Password\">\n  </div>\n</div>\n<div *ngIf=\"notAuth\">\n  <p style=\"font-family: Roboto; font-weight: 900; color: #ef4a4e; margin-left: 15%;\">Incorrect Details</p>\n</div>\n<div style=\"float: right; padding-right: 15%\">\n  <a [routerLink]=\"[ '/login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Forgot Password?</p>\n  </a>\n</div>\n<div style=\"margin-top: 12%; padding: 0% 15% 0% 15%;\">\n  <button type=\"submit\" class=\"btn btn-lg\" (click)=sendData() style=\"width: 100%; border-radius: 0px; background-color: #24242e; color:white\">Login</button>\n</div>\n<div style=\"float: right; padding-right: 15%; margin-top: 3%;\">\n  <a [routerLink]=\"[ '/user-login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Are You A User?</p>\n  </a>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/sign-org/sign-org.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignOrgComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignOrgComponent = (function () {
    function SignOrgComponent(dataService, route, router, spinnerService) {
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.notAuth = false;
        localStorage.clear();
    }
    SignOrgComponent.prototype.ngOnInit = function () {
    };
    SignOrgComponent.prototype.sendData = function () {
        var _this = this;
        this.spinnerService.show();
        var obj = {
            email: this.email,
            password: this.password
        };
        this.dataService.logInOrgs(obj).subscribe(function (response) {
            if (response.status === true) {
                localStorage.clear();
                localStorage.setItem('userId', 'org');
                localStorage.setItem('token', JSON.stringify(response.token));
                localStorage.setItem('name', response.user.fullname);
                localStorage.setItem('email', response.user.email);
                _this.user = JSON.parse(localStorage.getItem('token'));
                window.location.replace('http://www.ticketmax.ng/dashboard');
                _this.spinnerService.hide();
            }
            else {
                _this.notAuth = true;
                _this.spinnerService.hide();
            }
        }, function (error) {
            console.log('There was an error signing in');
            _this.notAuth = true;
            _this.spinnerService.hide();
        });
    };
    SignOrgComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sign-org',
            template: __webpack_require__("../../../../../src/app/components/sign-org/sign-org.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/sign-org/sign-org.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], SignOrgComponent);
    return SignOrgComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/sign-user/sign-user.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".notAuth {\r\n    box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(219, 58, 9, 0.781)\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/sign-user/sign-user.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"margin-top: 2%;\">\n  <h2 style=\"font-family: Roboto; color: #24242e; text-align: center; font-weight: 700;\">Sign in to your account</h2>\n</div>\n<div style=\"margin-top: 10%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"email\" placeholder=\"Email Address\">\n  </div>\n</div>\n<div style=\"margin-top: 6%; padding: 0% 15% 0% 15%;\">\n  <div class=\"form-group\">\n    <input [ngClass]=\"{'notAuth': notAuth}\" style=\"border-radius: 0px;\" type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=\"password\"\n      placeholder=\"Password\">\n  </div>\n</div>\n<div *ngIf=\"notAuth\">\n  <p style=\"font-family: Roboto; font-weight: 900; color: #ef4a4e; margin-left: 15%;\">Incorrect Details</p>\n</div>\n<div style=\"float: right; padding-right: 15%\">\n  <a [routerLink]=\"[ '/login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Forgot Password?</p>\n  </a>\n</div>\n<div style=\"margin-top: 12%; padding: 0% 15% 0% 15%;\">\n  <button type=\"submit\" class=\"btn btn-lg\" (click)=sendData() style=\"width: 100%; border-radius: 0px; background-color: #24242e; color:white\">Login</button>\n</div>\n<div style=\"float: right; padding-right: 15%; margin-top: 3%;\">\n  <a [routerLink]=\"[ '/org-login' ]\" style=\"text-decoration: none;\">\n    <p style=\"font-family: Roboto; font-weight: 900\">Are You An Organiser?</p>\n  </a>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/sign-user/sign-user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignUserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignUserComponent = (function () {
    function SignUserComponent(dataService, route, router, spinnerService) {
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.notAuth = false;
        localStorage.clear();
    }
    SignUserComponent.prototype.ngOnInit = function () {
    };
    SignUserComponent.prototype.sendData = function () {
        var _this = this;
        this.spinnerService.show();
        var obj = {
            email: this.email,
            password: this.password
        };
        this.dataService.logInUsers(obj).subscribe(function (response) {
            if (response.status === true) {
                // console.log(response.user.fullname);
                localStorage.clear();
                localStorage.setItem('userId', 'user');
                localStorage.setItem('token', JSON.stringify(response.token));
                localStorage.setItem('name', response.user.fullname);
                localStorage.setItem('email', response.user.email);
                _this.user = JSON.parse(localStorage.getItem('token'));
                window.location.replace('http://www.ticketmax.ng/');
                _this.spinnerService.hide();
            }
            else {
                _this.notAuth = true;
                _this.spinnerService.hide();
            }
        }, function (error) {
            console.log('There was an error signing in');
            _this.notAuth = true;
            _this.spinnerService.hide();
        });
    };
    SignUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sign-user',
            template: __webpack_require__("../../../../../src/app/components/sign-user/sign-user.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/sign-user/sign-user.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], SignUserComponent);
    return SignUserComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/ticket/ticket.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n.nav>li>a:focus,\r\n.nav>li>a:hover {\r\n  background-color: #24242e;\r\n  color: white;\r\n}\r\n\r\n.btn.focus,\r\n.btn:focus,\r\n.btn:hover {\r\n  color: white;\r\n}\r\n\r\n.modBox {\r\n    margin-bottom: 3%;\r\n}\r\n\r\n#resbox {\r\n  margin-left: -3%;\r\n}\r\n\r\n@media only screen and (min-width: 600px) {\r\n  #resbox {\r\n    margin-left: -1.7%;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  .scaleBox {\r\n    width: 50% !important;\r\n    height: 50% !important;\r\n    margin-left: 23% !important;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 600px) {\r\n  .scaleimg {\r\n    width: 100% !important;\r\n    height: 100% !important;\r\n  }\r\n}\r\n\r\n.btn-red {\r\n  background-image: none !important;\r\n  background-color: #ef4a4e !important;\r\n}\r\n\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/ticket/ticket.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height: 100%\">\n  <!-- Top Navigation Bar -->\n  <app-navbar></app-navbar>\n  <!-- Side Image -->\n  <div style=\"margin-top: -1.3%;\">\n    <div class=\"row\">\n      <div class=\"col-md-7 col-xs-12\">\n        <div class=\"scaleBox\" id=\"resBox\" style=\"height: 100%; width: 100%;\">\n          <div style=\"background-color: black;\">\n            <img class=\"scaleimg\" src=\"{{events.ref_image_url}}\" alt=\"\" style=\"opacity: 0.3; height: 846px; width: 910px; overflow: hidden;\">\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-5 col-xs-0\">\n        <div  style=\"font-family: Roboto; color: #24242e; padding-left: 5%; margin-top: 5%; padding-right: 5%;\">\n          <h2 style=\"font-weight: 900;\">{{events.name}}</h2>\n          <p style=\"font-size: 16px; margin-top: 4%\">By <span style=\"font-weight: 600\">King Ola Brand</span></p>\n          <div style=\"margin-top: 4%;\">\n            <span><img style=\"display: inline-flex;\" class=\"img-responsive\" src=\"assets/icons/red loc.png\" width=\"20px\" height=\"20px\" alt=\"\"></span>\n            <span style=\"display: inline-flex; margin-left: 1%;\">\n              <p style=\"font-size: 14px;\">{{events.loc}}</p>\n            </span>\n          </div>\n          <div style=\"margin-top: 2%;\">\n            <span><img style=\"display: inline-flex;\" class=\"img-responsive\" src=\"assets/icons/red cal.png\" width=\"20px\" height=\"20px\" alt=\"\"></span>\n            <span style=\"display: inline-flex; margin-left: 1%;\">\n              <p style=\"font-size: 14px;\">{{events.date}}</p>\n            </span>\n          </div>\n          <div>\n            <p style=\"margin-top: 7%;\">\n              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iure magnam veritatis aperiam dolores iusto! Obcaecati, eum, quae fugiat dignissimos accusantium iste laboriosam voluptatibus praesentium omnis voluptas iure magni! Rem, sed!\n              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sequi ullam explicabo earum iste voluptas ut unde nulla doloribus. Eum voluptatibus eos eligendi porro officia nostrum dolorem doloremque velit consectetur magnam.\n              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Similique nam quod, dolorem, tenetur quas, beatae ea dignissimos saepe et laborum molestias sed repudiandae doloremque perspiciatis vel eum quasi. Alias, eligendi!\n            </p>\n          </div>\n          <div style=\"margin-top: 17%; text-align: center; margin-bottom: 9%\">\n            <a style=\"color: white;\"><button class=\"btn btn-sm\" data-toggle=\"modal\" data-target=\"#myModal1\" style=\"border-radius: 8px; background-color: #ef4a4e; box-shadow: 2px 2px 78px -10px #ef4a4e; padding: 10px 30px;\" (click)=\"purchaseTicket(events.id)\">Purchase Ticket</button></a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <!-- Trigger the modal with a button -->\n  <!-- <button type=\"button\" class=\"btn btn-info btn-lg\">Open Modal</button> -->\n\n  <!-- Modal -->\n  <div *ngIf=\"user && !userToggle\" id=\"myModal1\" class=\"modal fade\" role=\"dialog\" style=\"margin-top: 7%;\">\n    <div class=\"modal-dialog\">\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n        <div class=\"modal-header\" style=\"background-color: #d8d8d8; border-radius: 5px;\">\n          <h4 class=\"modal-title\" style=\"text-align: center; font-weight: 700; font-family: Roboto\">Tickets</h4>\n        </div>\n        <div *ngFor=\"let ticket of tickets; let i = index\">\n          <div class=\"modal-body\" style=\"font-family: Roboto\">\n            <div class=\"modBox\" style=\"background-color: #24242e; padding-left: 5%; box-shadow: 0px 10px 40px -10px #24242e;\">\n              <div style=\"background-color: white; padding: 3% 3% 0% 3%;\">\n                <div class=\"row\">\n                  <div class=\"col-md-8 col-xs-8\">\n                    <p style=\"font-weight: 700\">{{ticket.title}}</p>\n                    <p><span style=\"font-weight: 700\">₦</span> {{ticket.price}}</p>\n                  </div>\n                  <!-- <div class=\"col-md-3 col-xs-6\">\n                    <div class=\"row\"></div>\n                  </div> -->\n                  <div class=\"col-md-3 col-xs-3\" style=\"float: right;\">\n                    <div class=\"row\" style=\"text-align: center;\">\n                      <div style=\"margin-top: 0%; margin-bottom: 3%; display: inline-flex;\">\n                        <div style=\"display: inline-flex;\" (click)=\"decrease(i)\">\n                          <img src=\"assets/icons/minus.png\" class=\"img-responsive\" style=\"height: 40px; width: 40px;\" alt=\"\">\n                        </div>\n                        <h4 style=\"padding-left: 4%; padding-right: 4%\">{{ticket.variable}}</h4>\n                        <div style=\"display: inline-flex; margin-left: 4%\" (click)=\"increase(ticket.limit, i)\">\n                          <img src=\"assets/icons/plus.png\" class=\"img-responsive\" style=\"height: 40px; width: 40px;\"alt=\"\">\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div>\n                  <hr style=\"margin-top: 0%\">\n                  <div style=\"margin-top: -2%; padding-bottom: 6%;\">\n                    <a data-dismiss=\"modal\" style=\"color: white;\">\n                      <div style=\"margin-top: -1%; float: right;\" (click)=\"getEventDet(ticket._id['$oid'], ticket.price, ticket.variable, events.id['$oid'])\">\n                        <angular4-paystack [key]=\"'pk_live_99da4e77652ec893008e4a6f0090aec042830086'\" [email]=\"email\"\n                            [amount]=\"ticket.total * 100\" [ref]=\"randomNum\"\n                            [class]=\"'btn btn-primary'\" (close)=\"paymentCancel()\"\n                            (callback)=\"paymentDone($event)\">\n                            Pay with Paystack\n                        </angular4-paystack>\n                      </div>\n                      <!-- <button class=\"btn btn-sm\" style=\"float: right; margin-top: -2px; border-radius: 6px; background-color: #ef4a4e; font-size: 9px; padding: 1% 3%;\">\n                        CHECKOUT\n                      </button> -->\n                    </a>\n                    <p style=\"font-weight: 700; float: left; margin-left: 0%; font-size: 16px;\">\n                      <span>Total: </span>₦ <span *ngIf=\"clicked\">{{ticket.price}}</span> <span *ngIf=\"!clicked\">{{ticket.total}}</span>\n                      <span></span>\n                    </p>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\" style=\"background-color: #d8d8d8; border-radius: 5px; padding-top: 3%;\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"foot\" style=\"background-color: #24242e\">\n  <div class=\"container-fluid\" style=\"font-family: Roboto;\">\n    <div class=\"container\">\n      <div class=\"row\" style=\"margin-top: 2%;\">\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">ticketMax</li>\n            <a style=\"text-decoration: none;\" [routerLink]=\"[ '/home' ]\">\n              <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Home</li>\n            </a>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Events</li>\n          </ul>\n        </div>\n        <div class=\"col-md-2 col-xs-4\">\n          <ul style=\"list-style-type: none;\">\n            <li style=\"font-weight: bold; color: white; margin-top: 8%\">Contact</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">FAQ</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Contact Us</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Facebook</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Twitter</li>\n            <li style=\"color: #9c9ca0; margin-top: 8%; font-weight: 300;\">Instagram</li>\n          </ul>\n        </div>\n        <div class=\"col-md-4 col-xs-0\"></div>\n        <div class=\"col-md-4 col-xs-12\">\n          <img class=\"img-responsive\" src=\"assets/images/wt.png\" alt=\"\">\n        </div>\n      </div>\n      <div>\n        <hr style=\"color: #9c9ca0\">\n      </div>\n    </div>\n    <div class=\"row\" style=\"margin-top: -1%;\">\n      <div class=\"col-md-4 col-xs-4\"></div>\n      <div class=\"col-md-4 col-xs-4\" style=\"display: inline-flex;\">\n        <span style=\"margin-right: 15%; margin-left: 25%;\">\n          <img class=\"img-responsive\" width=\"30px\" height=\"30px\" src=\"assets/icons/facebook.png\" alt=\"\">\n        </span>\n        <span style=\"margin-right: 15%;\">\n          <img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/tweet.png\" alt=\"\">\n        </span>\n        <span style=\"margin-right: 15%;\">\n          <img class=\"img-responsive\" width=\"25px\" height=\"25px\" src=\"assets/icons/insta.png\" alt=\"\">\n        </span>\n      </div>\n      <div class=\"col-md-4 col-xs-4\"></div>\n    </div>\n    <div style=\"text-align: center; font-family: Roboto; color: #9c9ca0; margin-top: 5%;\">\n      &copy;\n      <span>Gatepass, 2018</span>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/ticket/ticket.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TicketComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TicketComponent = (function () {
    function TicketComponent(dataService, route, router, spinnerService) {
        var _this = this;
        this.dataService = dataService;
        this.route = route;
        this.router = router;
        this.spinnerService = spinnerService;
        this.events = {};
        this.clicked = true;
        this.email = localStorage.getItem('email');
        this.user = localStorage.getItem('token');
        this.userType = localStorage.getItem('userId');
        this.randomNum = Math.floor((Math.random() * 1000000000) + 1);
        // console.log(this.randomNum);
        // this.events = this.dataService.getEvent();
        this.dataService.getEventDetails(this.route.snapshot.paramMap.get('id')).subscribe(function (events) {
            console.log(events);
            _this.events = events;
        });
        // tslint:disable-next-line:forin
        for (var key in this.events) {
            this.process_str = this.events['image'];
            this.events['ref_image_url'] = this.process_str.split('.jpg')[0] + 'l.jpg';
        }
        if (this.userType === 'user') {
            this.userToggle = false;
        }
        else {
            this.userToggle = true;
        }
    }
    TicketComponent.prototype.ngOnInit = function () {
    };
    TicketComponent.prototype.paymentCancel = function () { };
    TicketComponent.prototype.paymentDone = function ($event) {
        // console.log($event);
        this.reference = $event.trxref;
        if ($event) {
            var obj = {
                table: this.table,
                total_price: this.total_price,
                quantity: this.quantity,
                event: this.event,
                ref: this.reference
            };
            // console.log(obj);
            this.dataService.finalPay(obj).subscribe(function (res) {
                if (res.status === 'success') {
                    alert('Sucessful');
                }
                else {
                    alert(res.message);
                }
            });
        }
    };
    TicketComponent.prototype.getEventDet = function (table, total_price, quantity, event) {
        this.table = table;
        this.total_price = total_price;
        this.quantity = quantity;
        this.event = event;
    };
    TicketComponent.prototype.purchaseTicket = function (id) {
        var _this = this;
        if (this.user && !this.userToggle) {
            this.dataService.getTickets(id['$oid']).subscribe(function (tickets) {
                _this.tickets = tickets;
                // tslint:disable-next-line:forin
                for (var key in _this.tickets) {
                    _this.tickets[key]['variable'] = 1;
                    _this.tickets[key]['total'] = 0;
                }
                console.log(_this.tickets);
            });
        }
        else {
            this.router.navigate(['/user-login']);
        }
    };
    TicketComponent.prototype.decrease = function (i) {
        this.price = this.tickets[i].price;
        if (this.tickets[i].variable > 1) {
            --this.tickets[i].variable;
            this.tickets[i].total = this.tickets[i].variable * this.price;
        }
        else {
            this.tickets[i].variable = 1;
        }
    };
    TicketComponent.prototype.increase = function (limit, i) {
        this.price = this.tickets[i].price;
        if (this.tickets[i].variable < limit) {
            ++this.tickets[i].variable;
            this.tickets[i].total = this.tickets[i].variable * this.price;
        }
        this.clicked = false;
    };
    TicketComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-ticket',
            template: __webpack_require__("../../../../../src/app/components/ticket/ticket.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/ticket/ticket.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_3_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], TicketComponent);
    return TicketComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DataService = (function () {
    function DataService(http) {
        this.http = http;
        this.eventContent = {};
        this.user = JSON.parse(localStorage.getItem('token'));
        this.userId = localStorage.getItem('userId');
        // console.log(environment.production);
        if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production === true) {
            this.apiUrl = 'https://txfoodng.herokuapp.com/api/v2';
        }
        else {
            this.apiUrl = 'https://txfoodngstaging.herokuapp.com/api/v2';
        }
    }
    DataService.prototype.getEvents = function () {
        return this.http.get(this.apiUrl + '/events/open').map(function (res) { return res.json(); });
    };
    DataService.prototype.sendTable = function (id, obj) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.user);
        return this.http.post(this.apiUrl + '/table/edit/' + id, obj, { headers: headers }).map(function (res) { return res.json(); });
    };
    DataService.prototype.finalPay = function (obj) {
        console.log(obj);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.user);
        return this.http.post(this.apiUrl + '/pay', obj, { headers: headers }).map(function (res) { return res.json(); });
    };
    DataService.prototype.toggleOn = function (id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.user);
        return this.http.put(this.apiUrl + '/table/status/' + id, { headers: headers }).map(function (res) { return res.json(); });
    };
    DataService.prototype.sendEvent = function (id, image, date, name, loc) {
        this.eventContent.id = id;
        this.eventContent.image = image;
        this.eventContent.date = date;
        this.eventContent.name = name;
        this.eventContent.loc = loc;
    };
    DataService.prototype.createEvents = function (obj) {
        // console.log(obj);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.user);
        return this.http.post(this.apiUrl + '/events', obj, { headers: headers }).map(function (res) { return res.json(); });
    };
    DataService.prototype.getEvent = function () {
        return this.eventContent;
    };
    DataService.prototype.getTicketLists = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.user);
        return this.http.get(this.apiUrl + '/tickets', { headers: headers }).map(function (res) { return res.json(); });
    };
    DataService.prototype.getEventDetails = function (id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.user);
        // console.log(this.id);
        return this.http.get(this.apiUrl + '/event/' + id, { headers: headers }).map(function (res) { return res.json(); });
    };
    DataService.prototype.displayEvents = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.user);
        return this.http.get(this.apiUrl + '/events', { headers: headers }).map(function (res) { return res.json(); });
    };
    DataService.prototype.getTickets = function (id) {
        console.log(id);
        return this.http.get(this.apiUrl + '/event/tickets/' + id).map(function (res) { return res.json(); });
    };
    DataService.prototype.registerUsers = function (obj) {
        return this.http.post(this.apiUrl + '/user/register', obj).map(function (res) { return res.json(); });
    };
    DataService.prototype.registerOrgs = function (obj) {
        return this.http.post(this.apiUrl + '/organiser/register', obj).map(function (res) { return res.json(); });
    };
    DataService.prototype.logInUsers = function (obj) {
        return this.http.post(this.apiUrl + '/user/login', obj).map(function (res) { return res.json(); });
    };
    DataService.prototype.logInOrgs = function (obj) {
        return this.http.post(this.apiUrl + '/organiser/login', obj).map(function (res) { return res.json(); });
    };
    DataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "../../../../../src/assets/images/back.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "back.0725ae7ca56e82bc56a5.png";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map